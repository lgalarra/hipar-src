#!/usr/bin/python3

import sys
import pandas as pd
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import collections
import re

def figure(data, error=True, title='Methods vs. mean RMSE',
           xaxis='RMSE reduction', outfile='rmse-reduction.png') :
    axis_font = {'size': '18'}
    y_pos = numpy.arange(len(data))
    fig, axes = plt.subplots(figsize=(20, 10))
    labelsAvgRMSE = [r'' + x[0] for x in data]
    y_pos = numpy.arange(len(labelsAvgRMSE))
    if error :
        bplot1 = axes.barh(y_pos, [x[1] for x in data],
                           xerr=[x[2] for x in data], align='center', color='green', ecolor='black')
    else :
        bplot1 = axes.barh(y_pos, [x[1] for x in data],
                           align='center', color='green', ecolor='black')

    axes.set_yticks(y_pos)
    axes.set_yticklabels(labelsAvgRMSE)
    axes.set_xlabel(xaxis, axis_font)
    axes.set_title(title)
    for label in (axes.get_xticklabels() + axes.get_yticklabels()):
        label.set_fontsize(18)

    plt.savefig(outfile)

def figure_whiskers(data, title, xaxis, outfile, log=False, mappings={}, ylabel='Observed reductions') :
    fig = plt.figure(figsize=(12, 6))
    axes = plt.axes([0.1, 0.3, 0.85, 0.7])
    orderedData = collections.OrderedDict(sorted(data.items(), key=lambda x: numpy.median(x[1])))
    all_data = list(orderedData.values())
    axes.boxplot(all_data)
    axes.yaxis.grid(True)
    if log :
        axes.set_yscale('log')
    axes.set_xticks([y + 1 for y in range(len(all_data))])
    # add x-tick labels
    plt.xticks([y + 1 for y in range(len(all_data))], [mappings[x] for x in orderedData.keys()],
               rotation=45)
    plt.ylim(-110, 110)
    for label in (axes.get_xticklabels() + axes.get_yticklabels()):
        label.set_fontsize(18)

    for label in axes.get_xticklabels():
        if label.get_text() in blackboxMethods :
            label.set_color('blue')


    plt.savefig(outfile, dpi=72.)
    plt.show()


metrics = ['mae', 'rmse']
metricName2MetricColumn = {'mae' : 'Avg_MAE_reduction', 'rmse' : 'Avg_RMSE_reduction'}
metricName2MetricColumn2 = {'rmse' : 'Median_RMSE_reduction', 'mae' : 'Median_MAE_reduction'}
textMappings = {'Gradient Boosting Trees' : 'GBT',
                'Random Forest' : 'RF',
                'Rule Fit + Hipar + RF' : 'Rfit+H',
                'Rule Fit' : 'Rfit',
                'hipar_1.0_weighted_omp_nd_greedy_ob_1_0_ex_0_15_corrected' : '$Hipar_{sd}$',
                'hipar_1.0_weighted_dynamic_nd_ob_1_0_ex_0_15_corrected': 'Hipar',
                'hipar_1.0_weighted_dynamic_nd_ob_0_0_ex_0_15_corrected': '$Hipar_f$',
                'cpxr' : 'cpxr',
                'Regression Tree' : 'RT',
                'Model Tree (lars)' : 'MT',
                'Model Tree N_Rules + 1 (lars)' : '$MT_H$',
                'Regression Tree N_Rules + 1' : '$RT_H$',
                'Hipar + RuleFit': 'H+Rfit',
}

textMappingsMAE = textMappings.copy()
textMappingsMAE['hipar_2.0_weighted_dynamic_nd_ob_1_0_ex_0_15_corrected'] = 'Hipar'
textMappingsMAE['hipar_2.0_weighted_omp_nd_greedy_ob_1_0_ex_0_15_corrected'] = '$Hipar_{sd}$'
del textMappingsMAE['hipar_1.0_weighted_dynamic_nd_ob_1_0_ex_0_15_corrected']
del textMappingsMAE['hipar_1.0_weighted_omp_nd_greedy_ob_1_0_ex_0_15_corrected']



interpretableMethods = ['hipar_1.0_weighted_dynamic_nd_ob_1_0_ex_0_15_corrected',
                        'Regression Tree 2 x N_Rules',
                        'Regression Tree N_Rules + 1',
                        'Model Tree N_Rules + 1 (lars)', 'Model Tree (lars)', 'Regression Tree',
                        'Rule Fit + Hipar', 'hipar_1.25_weighted_omp_nd_greedy_ob_1_0_ex_0_15_corrected', 'cpxr',
                        'hipar_2.0_weighted_dynamic_nd_ob_1_0_ex_0_15', 'Rule Fit + Hipar + RF',
                        'hipar_1.25_weighted_dynamic_nd_ob_0_0', 'hipar_1.0_weighted_dynamic_nd_ob_0_0_ex_0_15_corrected']

blackboxMethods = ['H+Rfit', 'RF', 'GBT', 'Rfit']

itextMappings = {}
for key, value in textMappings.items():
    itextMappings[value] = key

countsPerMetric = {'mae': {}, 'rmse': {}}

def output_performance_charts(expData) :
    # Getting the ranks
    datasets = expData['Dataset'].unique()
    rankingsMAE = []
    rankingsRMSE = []
    rankingsMedianRMSE = []
    expData['interpretable'] = expData['Method'].apply(lambda x : x in interpretableMethods)
    expDataInterpretable = expData[expData['interpretable'] == True]
    expDataInterpretable['interpretability_index'] = pd.to_numeric(expDataInterpretable['nRules']) * \
                                                     (pd.to_numeric(expDataInterpretable['nCoefficients'])
                                                      + pd.to_numeric(expDataInterpretable['Number_of_conditions']))
    ## Performances per dataset
    for dataset in datasets:
        ## Prediction performance aggregations
        expDataForDataset = expData[expData.Dataset == dataset]

        expDataForDatasetSortedMAE = expDataForDataset.groupby('Method').agg({'Median_MAE_reduction' : ['median']})\
            ['Median_MAE_reduction'].sort_values('median', ascending=False)

        expDataForDatasetSortedRMSE = expDataForDataset.groupby('Method')\
            .agg({'Avg_RMSE_reduction': ['mean']})['Avg_RMSE_reduction'].sort_values('mean', ascending=False)

        expDataForDatasetSortedMedianRMSE = expDataForDataset.groupby('Method')\
            .agg({'Median_RMSE_reduction': ['median']})['Median_RMSE_reduction'].sort_values('median', ascending=False)


        expDataForDatasetSortedMAE4Output = list(map(lambda x: (textMappingsMAE[x[0]], x[1]['median']),
                                         filter(lambda x: x[0] in textMappingsMAE, expDataForDatasetSortedMAE.iterrows())))

        expDataForDatasetSortedRMSE4Output = list(map(lambda x: (textMappings[x[0]], x[1]['mean']),
                                         filter(lambda x: x[0] in textMappings, expDataForDatasetSortedRMSE.iterrows())))

        expDataForDatasetSortedMedianRMSE4Output = list(map(lambda x: (textMappings[x[0]], x[1]['median']),
                                                      filter(lambda x: x[0] in textMappings,
                                                             expDataForDatasetSortedMedianRMSE.iterrows())))

        figure(expDataForDatasetSortedMAE4Output, error=False,
               title='Methods vs. median median MeAE reduction (' + dataset +')', xaxis='Median MeAE reduction',
               outfile=dataset+'-mae-reduction.png')

        figure(expDataForDatasetSortedRMSE4Output, error=False,
               title='Methods vs. mean mean RMSE reduction (' + dataset + ')', xaxis='Mean RMSE reduction',
               outfile=dataset + '-rmse-reduction.png')

        figure(expDataForDatasetSortedMedianRMSE4Output, error=False,
               title='Methods vs. median median RMSE reduction (' + dataset + ')', xaxis='Median RMSE reduction',
               outfile=dataset + '-median-rmse-reduction.png')

        rankingRMSE = list(filter(lambda x: x in textMappings, expDataForDatasetSortedRMSE.index))
        rankingMAE = list(filter(lambda x: x in textMappingsMAE, expDataForDatasetSortedMAE.index))
        rankingMedianRMSE = list(filter(lambda x: x in textMappings, expDataForDatasetSortedMedianRMSE.index))
        rankingsRMSE.append(rankingRMSE)
        rankingsMAE.append(rankingMAE)
        rankingsMedianRMSE.append(rankingMedianRMSE)

        ## Interpretability aggregations
        expDataInterpretableForDataset = expDataInterpretable[expDataInterpretable.Dataset == dataset]
        expDataInterpretableForDatasetSorted = expDataInterpretableForDataset.groupby('Method').agg({'interpretability_index': ['median']}) \
            ['interpretability_index'].sort_values('median', ascending=False)

        expDataInterpretableForDatasetSorted4Output = list(map(lambda x: (textMappings[x[0]], x[1]['median']),
                 filter(lambda x: x[0] in textMappings, expDataInterpretableForDatasetSorted.iterrows())))

        figure(expDataInterpretableForDatasetSorted4Output, error=False,
               title='Methods vs. complexity (' + dataset + ')', xaxis='Complexity',
               outfile=dataset + '-complexity.png')



    ## First let us output the rankings
    methods = expData['Method'].unique()
    method2AvgRMSE = []
    method2AvgMAE = []
    method2MedianRankOnRMSE = {}
    method2MedianRankOnMAE = {}
    method2MedianRankOnMedianRMSE = {}

    method2TrainingTime = []
    for method in methods :
        expData4Method = expData[expData.Method == method]
        method2TrainingTime.append(
            (method, expData4Method.agg({'Training_time': ['mean']}).loc['mean', 'Training_time'])
        )

        for metric in metrics :
            expData4Method = expData[expData.Method == method]
            expData4Method = expData4Method.loc[(expData4Method['Metric2Optimize'] == metric) | (expData4Method['Metric2Optimize'] == '*') ]


            # Getting the central tendency measures
            column2Agg = metricName2MetricColumn[metric]
            column2Agg2 = metricName2MetricColumn2[metric]
            aggregatedData = expData4Method.agg({column2Agg: ['mean', 'median', 'count'],
                                                 column2Agg2 : ['mean', 'median', 'count'],
                                                 'Std_RMSE_reduction' : ['mean'],
                                                 'Median_RMSE_reduction' : ['median']})


            if metric == 'mae' :
                method2AvgMAE.append((method, aggregatedData.loc['mean', column2Agg2]))
                countsPerMetric[metric][method] = aggregatedData.loc['count', column2Agg2]
                if method in textMappingsMAE:
                    method2MedianRankOnMAE[method] = []
                    for ranking in rankingsMAE:
                        method2MedianRankOnMAE[method].append(ranking.index(method) + 1)
            else:
                method2AvgRMSE.append((method, aggregatedData.loc['mean', column2Agg],
                                       aggregatedData.loc['mean', 'Std_RMSE_reduction'],
                                       aggregatedData.loc['median', column2Agg],
                                       aggregatedData.loc['median', 'Median_RMSE_reduction']))

                countsPerMetric['rmse'][method] = aggregatedData.loc['count', column2Agg]
                if method in textMappings:
                    method2MedianRankOnRMSE[method] = []
                    for ranking in rankingsRMSE:
                        method2MedianRankOnRMSE[method].append(ranking.index(method) + 1)

                    method2MedianRankOnMedianRMSE[method] = []
                    for ranking in rankingsMedianRMSE :
                        method2MedianRankOnMedianRMSE[method].append(ranking.index(method) + 1)


    method2TrainingTime4Output = list(map(lambda x : x, filter(lambda x: x[0] in textMappings, method2TrainingTime)))

    ### Plotting average values

    print('RMSE')
    method2AvgRMSE = sorted(method2AvgRMSE, key=lambda x : x[1], reverse=True)
    method2AvgRMSE4Output = list(map(lambda x : (textMappings[x[0]], x[1], x[2], x[3]),
                                     filter(lambda x: x[0] in textMappings, method2AvgRMSE)))
    print(method2AvgRMSE4Output)

    method2MedianRMSE = sorted(method2AvgRMSE, key=lambda x : x[3], reverse=True)
    method2MedianRMSE = list(map(lambda x : (textMappings[x[0]], x[3]),
                                     filter(lambda x: x[0] in textMappings, method2MedianRMSE)))
    #print(method2MedianRMSE)

    aggRankingOnRMSE = []
    for method in method2MedianRankOnRMSE :
        aggRankingOnRMSE.append((method, numpy.median(method2MedianRankOnRMSE[method])))

    aggRankingOnRMSE = sorted(aggRankingOnRMSE, key=lambda x : x[1])
    #print(aggRankingOnRMSE)


    aggRankingOnMedianRMSE = []
    for method in method2MedianRankOnMedianRMSE :
        aggRankingOnMedianRMSE.append((method, numpy.median(method2MedianRankOnMedianRMSE[method])))
    aggRankingOnMedianRMSE = sorted(aggRankingOnMedianRMSE, key=lambda x : x[1])
    #print(aggRankingOnMedianRMSE)

    #print('MAE')
    method2AvgMAE = sorted(method2AvgMAE, key=lambda x : x[1], reverse=True)
    method2AvgMAE = list(map(lambda x : (textMappingsMAE[x[0]], x[1]),
                             filter(lambda x: x[0] in textMappingsMAE, method2AvgMAE)))
    #print(method2AvgMAE)


    aggRankingOnMAE = []
    for method in method2MedianRankOnMAE :
        aggRankingOnMAE.append((method, numpy.median(method2MedianRankOnMAE[method])))

    aggRankingOnMAE = sorted(aggRankingOnMAE, key=lambda x : x[1])
    #print(aggRankingOnMAE)

    # Plotting average RMSE
    figure(method2AvgRMSE4Output)
    figure(method2MedianRMSE, error=False, title='Methods vs. median RMSE reduction',
           xaxis='RMSE reduction', outfile='median-rmse-reduction.png')


    # Plotting average MAE
    figure(method2AvgMAE, error=False, title='Methods vs. median MeAE reduction',
           xaxis='MeAE reduction', outfile='mae-reduction.png')


    # Plotting runtime
    #print('Runtime')
    method2TrainingTime4Output = sorted(method2TrainingTime4Output, key=lambda x: x[1])
    axis_font = {'size': '18'}
    excluded = ['Model Tree N_Rules + 1 (lars)', 'Regression Tree N_Rules + 1']
    fig = plt.figure(figsize=(10, 5))
    axes = plt.axes([0.1, 0.3, 0.85, 0.5])
    x_pos = numpy.arange(len(method2TrainingTime4Output) - len(excluded))
    bplot2 = axes.bar(x_pos, [x[1] for x in method2TrainingTime4Output if x[0] not in excluded])
    #axes.set_xticks([textMappings[x[0]] for x in method2TrainingTime4Output])
    plt.xticks(x_pos, [textMappings[x[0]] for x in method2TrainingTime4Output if x[0] not in excluded], rotation=45)
    axes.set_yscale('log')
    #axes.set_title('Runtime')
    #axes.set_xlabel('Method')
    axes.set_ylabel('seconds', axis_font)
    for label in (axes.get_xticklabels() + axes.get_yticklabels()):
        label.set_fontsize(18)
    plt.savefig('runtime.png')


    ### Plotting rankings
    with open('out.tex', 'w') as f:
        f.write("\\documentclass[a4paper,10pt]{article}\n\\usepackage[utf8]{inputenc}\n\\usepackage{booktabs}\n\\begin{document}\n\\title{Charts}\\maketitle\n")

        f.write("\\begin{table}\n\\begin{tabular}{cc}\n\\\\Method & Median rank on mean RMSE \\\\ \\toprule \n")
        for p in aggRankingOnRMSE:
            f.write('\t\t' + textMappings[p[0]] + '&' + str(p[1]) + '\\\\ \n')
        f.write('\\end{tabular}\\end{table}')

        f.write("\\begin{table}\n\\begin{tabular}{cc}\n\\\\Method & Median rank on median MeAE \\\\ \\toprule \n")
        for p in aggRankingOnMAE:
            f.write('\t\t' + textMappingsMAE[p[0]] + '&' + str(p[1]) + '\\\\ \n')
        f.write('\\end{tabular}\\end{table}')

        f.write("\\begin{table}\n\\begin{tabular}{cc}\n\\\\Method & Median rank on median RMSE \\\\ \\toprule \n")
        for p in aggRankingOnMedianRMSE:
            f.write('\t\t' + textMappings[p[0]] + '&' + str(p[1]) + '\\\\ \n')
        f.write('\\end{tabular}\\end{table}')

        f.write("\\end{document}")

    ## Plotting interpretability
    method2ComplexityRMSE = []
    method2ComplexityMAE = []
    for method in expDataInterpretable['Method'].unique() :
        for metric in metrics :
            expDataInterpretable4Method = expDataInterpretable[expDataInterpretable.Method == method]
            expDataInterpretable4Method = expDataInterpretable4Method.loc[(expDataInterpretable4Method['Metric2Optimize'] == metric)
                                                                          | (expDataInterpretable4Method['Metric2Optimize'] == '*') ]


            # Getting the central tendency measures
            aggregatedData = expDataInterpretable4Method.agg({'interpretability_index' : ['median']})
            if metric == 'mae' :
                method2ComplexityMAE.append((method, aggregatedData.loc['median', 'interpretability_index']))
            else :
                method2ComplexityRMSE.append((method, aggregatedData.loc['median', 'interpretability_index']))


    method2FullReductionsRMSE = {}
    for method in textMappings.keys() :
        method2FullReductionsRMSE[method] = []
        expData4Method = expData[expData.Method == method]
        expData4Method = expData4Method.loc[(expData4Method['Metric2Optimize'] == 'rmse')
                                                                      | (expData4Method['Metric2Optimize'] == '*') ]

        for dataset in datasets :
            for index, cells in expData4Method[expData4Method.Dataset == dataset].iterrows() :
                rmseVals = sorted(map(float, cells['Scores_RMSE_reduction'].split(',')))
                method2FullReductionsRMSE[method].extend(rmseVals)
                #method2FullReductionsRMSE[method].extend(rmseVals[1:len(rmseVals)-1])

    method2FullReductionsMAE = {}
    for method in textMappingsMAE.keys() :
        method2FullReductionsMAE[method] = []
        expData4Method = expData[expData.Method == method]
        expData4Method = expData4Method.loc[(expData4Method['Metric2Optimize'] == 'mae')
                                                                      | (expData4Method['Metric2Optimize'] == '*') ]

        for dataset in datasets :
            for index, cells in expData4Method[expData4Method.Dataset == dataset].iterrows() :
                method2FullReductionsMAE[method].extend(map(float, cells['Scores_MAE_reduction'].split(',')))


    complexityDataDict = {'rmse' : method2ComplexityRMSE, 'mae' : method2ComplexityMAE}
    textMappingsMAE2 = textMappingsMAE.copy()
    textMappingsMAE2['hipar_1.0_weighted_dynamic_nd_ob_1_0_ex_0_15_corrected'] = '$Hipar_{\sigma=1}$'
    textMappingsMAE2['hipar_2.0_weighted_dynamic_nd_ob_1_0_ex_0_15_corrected'] = '$Hipar_{\sigma=2}$'
    mappingsDict = {'rmse' : textMappings, 'mae' : textMappingsMAE2}
    excluded = ['Model Tree N_Rules + 1 (lars)', 'Regression Tree N_Rules + 1', 'hipar_1.25_weighted_omp_nd_greedy_ob_1_0_ex_0_15']
    for metric in complexityDataDict  :
        method2Complexity4Output = list(map(lambda x : x, filter(lambda x: x[0] in mappingsDict[metric] and x[0] not in excluded, complexityDataDict[metric])))
        method2Complexity4Output = sorted(method2Complexity4Output, key=lambda x : x[1])
        fig = plt.figure(figsize=(10, 5))
        axes = plt.axes([0.1, 0.3, 0.85, 0.5])
        #fig, axes = plt.subplots(figsize=(10, 9))
        x_pos = numpy.arange(len(method2Complexity4Output))
        bplot4 = axes.bar(x_pos, [x[1] for x in method2Complexity4Output])
        plt.xticks(x_pos, [mappingsDict[metric][x[0]] for x in method2Complexity4Output], rotation=30)
        axes.set_yscale('log')
        #axes.set_title('Complexity')
        #axes.set_xlabel('Method')
        axes.set_ylabel('# elements', axis_font)
        for label in (axes.get_xticklabels() + axes.get_yticklabels()):
            label.set_fontsize(18)

        plt.savefig('complexity_' + metric + '.png')

    #print(method2FullReductionsRMSE)
    #print(method2FullReductionsMAE)

    # adding horizontal grid lines
    figure_whiskers(method2FullReductionsRMSE, 'RMSE reduction', 'Regression Methods', 'whiskers_rmse.png', log=False, mappings=textMappings, ylabel='RMSE reduction')
    figure_whiskers(method2FullReductionsMAE, 'MAE reduction', 'Regression Methods', 'whiskers_mae.png', log=False, mappings=textMappingsMAE, ylabel='MeAE reduction')

def extractOmegaGBR(methodName) :
    matchObj = re.match('Gradient Boosting Rules \(sb=1.25, ob=(\d+\.\d+)\)', methodName)
    return float(matchObj.group(1))

def extractOmega(methodName) :
    #hipar_1.25_weighted_dynamic_nd_ob_0_0
    return float(methodName.replace('hipar_1.25_weighted_dynamic_nd_ob_', '').replace('_', '.'))

def extractSigma(methodName) :
    #hipar_1.25_weighted_dynamic_nd_ob_0_0
    matchObj = re.match('hipar_(\d+\.\d+)_weighted_dynamic_nd_ob_1_0', methodName)
    return float(matchObj.group(1))

def twoYAxesFigure(x, y1, y2, xlabel, ylabel1, ylabel2, outfileName, stdev=None) :
    #fig, ax1 = plt.subplots(figsize=(10, 5))
    fig = plt.figure(figsize=(10, 5))
    ax1 = plt.axes([0.1, 0.2, 0.6, 0.6])

    color = 'tab:red'
    x_axis_font = {'size': '18'}
    axis_font = {'size': '18', 'color' : color}
    ax1.set_xlabel(xlabel, x_axis_font)
    ax1.set_ylabel(ylabel1, axis_font)
    ax1.plot(x, y1, color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.set_ylim([min(y1) - 10, max(y1) + 10])

    for label in (ax1.get_yticklabels() +  ax1.get_xticklabels()):
        label.set_fontsize(18)

    if stdev is not None:
        stdevarr = numpy.array(stdev)
        plt.fill_between(x, y1 - stdevarr, y1 + stdevarr, color='gray', alpha=0.2)


    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    axis_font['color'] = 'tab:blue'
    ax2.set_ylabel(ylabel2, axis_font)  # we already handled the x-label with ax1
    ax2.plot(x, y2, color=axis_font['color'])
    ax2.tick_params(axis='y', labelcolor=axis_font['color'])

    for label in ax2.get_yticklabels():
        label.set_fontsize(18)


    #fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(outfileName)


def output_parameter_evolution_charts(expData1, expData2, expData3) :
    ## Add a column with the sigma value
    expData1['omega'] = numpy.zeros(len(expData1))
    expData1['sigma'] = numpy.zeros(len(expData1))
    expData1['rmse_mean'] = numpy.zeros(len(expData1))
    for index, row in expData1.iterrows() :
        ## Extract the sigma value from the method
        expData1.at[index, 'sigma'] = extractSigma(row['Method'])
        newV = []
        for v in row['Scores_RMSE_reduction'].split(',') :
            newV.append(float(v))

        newV.sort()
        newV = newV[2: len(newV) - 2]
        expData1.at[index, 'rmse_mean'] = numpy.mean(newV)

    expData1['interpretability_index'] = expData1['nRules'] * \
                                         (expData1['nCoefficients'] + expData1['Number_of_conditions'])

    expData2['omega'] = numpy.zeros(len(expData2))
    expData2['sigma'] = numpy.full(len(expData2), 1.25)
    expData2['rmse_mean'] = numpy.zeros(len(expData2))


    for index, row in expData2.iterrows() :
        newV = []
        for v in row['Scores_RMSE_reduction'].split(',') :
            newV.append(float(v))

        newV.sort()
        newV = newV[2: len(newV) - 2]
        expData2.at[index, 'omega'] = extractOmega(row['Method'])
        expData2.at[index, 'rmse_mean'] = numpy.mean(newV)

    expData2['interpretability_index'] = expData2['nRules'] * \
                                         (expData2['nCoefficients'] + expData2['Number_of_conditions'])

    expData3['omega'] = numpy.zeros(len(expData3))
    expData3['sigma'] = numpy.full(len(expData3), 1.25)
    expData3['rmse_mean'] = numpy.zeros(len(expData3))

    for index, row in expData3.iterrows():
        if not row['Method'].startswith('Gradient') :
            continue

        newV = []
        for v in row['Scores_RMSE_reduction'].split(','):
            newV.append(float(v))

        newV.sort()
        newV = newV[2: len(newV) - 2]
        expData3.at[index, 'omega'] = extractOmegaGBR(row['Method'])
        expData3.at[index, 'rmse_mean'] = numpy.mean(newV)

    ##Now do the corresponding aggregations to generate charts per each sigma
    ##and omega values.
    aggregatedData1 = expData1.groupby('sigma').agg({'rmse_mean': ['mean', 'std'], 'interpretability_index' : ['mean']})
    aggregatedData2 = expData2.groupby('omega').agg({'Bagging_rate' : ['mean'],  'rmse_mean': ['mean', 'std'], 'interpretability_index' : ['mean']})
    aggregatedData3 = expData3.groupby('omega').agg({'rmse_mean': ['mean']})

    xsigma = []
    y1 = []
    y2 = []
    for index, row in aggregatedData1['rmse_mean'].iterrows() :
        xsigma.append(index)
        y1.append(row['mean'])
        y2.append(aggregatedData1.loc[index, 'interpretability_index'])

    twoYAxesFigure(xsigma, y1, y2, '$\sigma$', 'Avg. RMSE reduction', '# elements', 'sigma_evolution.png')

    xomega = []
    y1 = []
    y2 = []
    for index, row in aggregatedData2['rmse_mean'].iterrows() :
        xomega.append(index)
        y1.append(row['mean'])
        y2.append(aggregatedData2.loc[index, 'interpretability_index'])


    twoYAxesFigure(xomega, y1, y2, '$\omega$', 'Avg. RMSE reduction', '# elements', 'omega_evolution.png')

    xomega = []
    y1 = []
    for index, row in aggregatedData2['Bagging_rate'].iterrows():
        xomega.append(index)
        y1.append(row['mean'])

    fig = plt.figure(figsize=(10, 5))
    ax1 = plt.axes([0.1, 0.2, 0.6, 0.6])
    x_axis_font = {'size': '18'}
    ax1.set_xlabel('$\omega$', x_axis_font)
    ax1.set_ylabel('rules per data point', x_axis_font)
    ax1.plot(xomega, y1)
    ax1.tick_params(axis='y')
    for label in (ax1.get_yticklabels() + ax1.get_xticklabels()):
        label.set_fontsize(18)
    plt.savefig('bagging_omega.png')


    xomega = []
    y1 = []
    for index, row in aggregatedData3['rmse_mean'].iterrows():
        xomega.append(index)
        y1.append(row['mean'])

    fig, ax1 = plt.subplots(figsize=(10, 5))

    ax1.set_xlabel('Omega')
    ax1.set_ylabel('RMSE reduction')
    ax1.plot(xomega, y1)
    ax1.tick_params(axis='y')
    ax1.set_ylim([min(y1) - 10, max(y1) + 10])
    plt.savefig('gradient_boosting_rules_omega.png')

if __name__ == "__main__":
    expData = pd.read_csv(sys.argv[1], sep='\t')

    output_performance_charts(expData)

    # Sigma variation
    if len(sys.argv) >= 5:
        expData1 = pd.read_csv(sys.argv[2], sep='\t')
        # Omega variation
        expData2 = pd.read_csv(sys.argv[3], sep='\t')
        # Omega variation for GBR
        expData3 = pd.read_csv(sys.argv[4], sep='\t')

        output_parameter_evolution_charts(expData1, expData2, expData3)
