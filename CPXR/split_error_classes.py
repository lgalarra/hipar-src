'''
Created on Mar 20, 2018

@author: lgalarra
'''
import numpy as np
import pandas as pd

class ErrorBasedSplitter(object):
    '''
    The instances of this class split a dataset represented as a data frame 
    into two classes: large error (LE) and small error (SE) based 
    on the magnitude of the error made by a given regression model 
    '''


    def __init__(self, data, splitRatio = 0.45):
        '''
        It initializes the object with a given split ratio.
        The split ratio is a value between 0 and 1 (the extremes excluded). Assuming the data
        points have been sorted in ascending order by the model's prediction error, 
        this factor determines the proportion of the model's total error 
        covered by the points in the LE class.
        '''
        if splitRatio <= 0.0 or splitRatio >= 1.0 :
            raise ValueError('splitRatio is a value between 0 and 1 (extremes excluded). Value provided: ' 
                             + str(splitRatio))
        self.data = data
        self.splitRatio = splitRatio


    def concat(self, le, se, attribute):    
        '''
        Given two dataframes returned by the method split, it concatenates them into a 
        single dataframe and adds an additional attribute with values 0 or 1 to trace the
        origin of the data points (0 if it belongs to le, 1 if it belongs to se)
        '''        
        discriminantAttribute = attribute
        le[discriminantAttribute] = pd.Series(1, index=le.index)
        se[discriminantAttribute] = pd.Series(0, index=se.index)
        return pd.concat([le, se])

    
    def split(self, targetVariable, regressionModel):
        '''
        It splits the given dataset in two parts based on the given split ratio.
        and the error made by the regression model on the target variable.
        The method uses the regression model to predict the target variable 
        based only on the numerical attributes in the data. The target attribute  
        must be numerical as well.
    
        '''
        dataHeaders = list(self.data.columns.values)
        if targetVariable not in dataHeaders :
            raise ValueError(targetVariable + ' is not a field in the data.')

        numericalHeaders = self.data.select_dtypes(include=[np.number]).columns.tolist()
         
        ## Let us get only the numerical columns
        x = self.data[numericalHeaders]
        y = x[targetVariable]
        del x[targetVariable]
        ## Learning a regression model
        predictionModel = regressionModel.fit(x, y)
        
        # Now compute the residuals
        yPredicted = predictionModel.predict(x)
        residuals = np.absolute(yPredicted - y)
        residualsColumnName = 'residuals_' + str(targetVariable)
        # Create temporary columns with the residuals
        self.data[residualsColumnName] = pd.Series(residuals, index=self.data.index)
        # Sort the data frame based on the residual
        sortedData = self.data.sort_values(residualsColumnName, ascending=False)
        ## Get the total error
        totalError = np.sum(residuals)
        # And the share of the total error that we consider as large error
        largeErrorShare = totalError * self.splitRatio
        cumulativeError = 0.0
        splitIndex = 0
        
        # Get the index point where the cumulative error is 
        # close to the large error share
        while (cumulativeError < largeErrorShare 
        and not np.isclose(np.abs(cumulativeError - largeErrorShare), 0.0, atol=10**-6) 
        and splitIndex < len(sortedData) - 1) :
            cumulativeError = cumulativeError + sortedData.iloc[splitIndex][residualsColumnName]
            splitIndex += 1

        largeErrorData = sortedData.iloc[:splitIndex]
        smallErrorData = sortedData.iloc[splitIndex:]
        
        # Clean the original data frame
        del self.data[residualsColumnName]
        
        return largeErrorData, smallErrorData, predictionModel