#!/usr/bin/env python3

import numpy

## Greedy approach that just takes the best q models in terms
## of ratio support/error (both metrics are normalized)
def select_best_models(models, data, q) :
    num_rules = len(models)
    errors = numpy.array([m[5] for m in models])
    supports = numpy.array([m[3] for m in models])
    errors = errors / sum(errors)
    errors = numpy.clip(errors, 0.005, 1)
    supports = supports / sum(supports)
    scores = errors / supports

    list2sort = []
    for i in range(len(models)) :
        list2sort.append((i, scores[i]))

    sortedList = sorted(list2sort, key=lambda x : x[1], reverse=True)
    chosen_models = []
    for i in range(min(q, len(sortedList))) :
        chosen_models.append(sortedList[i][0])

    return chosen_models