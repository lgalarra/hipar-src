"""
 lars_regression.py  (author: Luis Galarraga)
"""

from sklearn.metrics import mean_squared_error
import warnings

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

class lars_regression:

    def __init__(self):
        from sklearn.linear_model import LassoLars
        self.model = LassoLars(alpha=.1, fit_path=False)

    def fit(self, X, y):
        self.model.fit(X, y)

    def predict(self, X):
        return self.model.predict(X)

    def loss(self, X, y, y_pred):
        return mean_squared_error(y, y_pred)