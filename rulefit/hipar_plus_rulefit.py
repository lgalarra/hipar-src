'''
Hipar hierarchical search for regions plus RuleFit boosting
strategy.
'''

import sys
import pandas
import numpy

from sklearn.linear_model import LassoCV
from sklearn.linear_model import LassoLars
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.feature_extraction import DictVectorizer

sys.path.append('../hipar_libs/')
from num_rules import closed_rule_search2
from num_rules import mine_num_rules2



class HIPARPlusRuleFit(object) :

    def __init__(self, min_support=0.02, error_metric='rmse') :
        self.min_support = min_support
        self.error_metric = error_metric
        self._models = None
        self._regressionModel = None
        self._vectorizer = None

    def _vectorize(self, X):
        for model in self._models :
            if len(model[0]) == 0 :
                continue
            labels = []
            for key, value in model[0].items() :
                label = value
                #X[label] = X.where(label, 0)
                if '<' not in label and '>' not in label :
                    label = label.replace("'", r"\'")
                    label = "X." + key + "=='" + label + "'"
                else:
                    ## For the discretized case
                    label = '(' + label.replace(key, 'X.' + key).replace(' and ', ') & (') + ')'

                labels.append(label)

            finalLabel = "(" + ") & (".join(labels) + ")"
            #print(finalLabel)
            X[finalLabel] = eval(finalLabel)
        return X


    def fit(self, X, y, params={}):
        Xp = X.copy()
        Xp['target'] = y
        Xp = Xp.reset_index(drop=True)
        symb_attr = list(X.select_dtypes(include='object'))
        num_attr = list(X.select_dtypes(include=['int64', 'float64']))

        if 'ignored_attrs' in params:
            ignored_attrs = params['ignored_attrs']
        else:
            ignored_attrs = []

        #symb_attr = [a for a in symb_attr if a not in ignored_attrs]
        #num_attr = [a for a in num_attr if a not in ignored_attrs]

        #self._models, default_model = closed_rule_search2(Xp, 'target', self.min_support,
        #                                                       num_attr, symb_attr,
        #                                                       discretize=True,
        #                                                       discretize_method='mdlp', discriminant_rules=True,
        #                                                       error_metric=self.error_metric,
        #                                                       reduce_output=True, closure_num=True)

        self._models = mine_num_rules2(Xp, 'target', self.min_support,
                                                          discretize=True, ignored_attrs=ignored_attrs,
                                                          discretize_method='mdlp',
                                                          error_metric=self.error_metric,
                                                          reduce_output=True, closure_num=True, n_rules=1,
                                                      reg_type="dynamic")
        removeIdx = -1
        for i in range(len(self._models)) :
            if self._models[0] == {} :
                removeIdx = i
        if removeIdx >= 0 :
            self._models.remove(removeIdx)

        print(len(self._models), 'models')
        X_vec = self._vectorize(X)

        self._regressionModel = LassoLars(alpha=0.1, fit_path=False)
        #self._regressionModel = LassoLars()
        #self._regressionModel = LinearRegression()
        self._vectorizer = DictVectorizer(sparse=False)
        X_vec_dict = X_vec.to_dict('records')
        #self._cleanDict(X_vec_dict, symb_attr)
        _X_ = self._vectorizer.fit_transform(X_vec_dict)
        _y_ = numpy.array(y)
        self._regressionModel.fit(_X_, _y_)
        return self._regressionModel

    def _cleanDict(self, all_dicts, attr_2_remove):
        for adict in all_dicts :
            for attr in attr_2_remove :
                del adict[attr]



    def predict(self, X):
        X_vec = self._vectorize(X)
        X_vec_dict = X_vec.to_dict('records')
        #self._cleanDict(X_vec_dict, X.select_dtypes(include='object'))
        _X_ = self._vectorizer.transform(X_vec_dict)
        return self._regressionModel.predict(_X_)

if __name__ == "__main__":
    #cpu_types = {'Data': str, 'Ext': str}
    X_cpu = pandas.read_csv('../datasets/servo.csv')
    X_cpu.dropna(axis=0)
    cpu_target = 'class'
    y_cpu = X_cpu[cpu_target]
    del X_cpu[cpu_target]

    learner = HIPARPlusRuleFit()
    learner.fit(X_cpu, y_cpu)


    yp = learner.predict(X_cpu)
    print(mean_squared_error(yp, numpy.array(y_cpu)))