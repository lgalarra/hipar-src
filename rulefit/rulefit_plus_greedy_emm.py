import sys
sys.path.append('../emm/')

import numpy
from sklearn.feature_extraction import DictVectorizer


from rulefit_plus_hipar import RuleFitPlusHIPAR
from greedy_selection import select_best_models

class RuleFitPlusGreedyEMM(RuleFitPlusHIPAR) :

    def __init__(self, tree_size = 4, sample_fract = 'default', max_rules = 2000, memory_par = 0.01,
                 tree_generator = None, rfmode = 'regress', lin_trim_quantile = 0.025, lin_standardise = True,
                 exp_rand_tree_size = True, model_type = 'rl', Cs = None, cv = 3, random_state = None,
                 support_bias = 0.5, min_support=0.02, number_final_rules=5) :
        ## Create the ruleFit instance that will do the rule search
        super().__init__(tree_size, sample_fract, max_rules, memory_par,
                         tree_generator, rfmode, lin_trim_quantile, lin_standardise,
                         exp_rand_tree_size, model_type, Cs, cv, random_state,
                         support_bias, min_support)
        self.number_final_rules = number_final_rules

    def fit(self, X, y):
        vec = DictVectorizer(sparse=False)
        _X_ = vec.fit_transform(X.to_dict('records'))
        _y_ = numpy.array(y)
        ## Learn the rules with RuleFit
        self._ruleFit.fit(_X_, _y_, vec.feature_names_)
        rules = self._ruleFit.get_rules()
        ## Bring those rules to the structure required by HIPAR
        oneAtomRules = rules[rules.type == 'linear']
        oneAtomRules = oneAtomRules[oneAtomRules['rule'].str.contains('=')]
        complexRules = rules[rules.type == 'rule']

        models = self._prepareRulesForHipar(complexRules, oneAtomRules, X, y)
        self._models = select_best_models(models, X, self.number_final_rules)
        return self