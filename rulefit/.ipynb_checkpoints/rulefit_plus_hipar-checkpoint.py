
import pandas
import numpy
import sys
import re

sys.path.append('../hipar_libs/')

from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import OrthogonalMatchingPursuit
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LassoLars
from sklearn.metrics import mean_squared_error
from sklearn.metrics import median_absolute_error
from rulefit import RuleFit
from diversity_utils import diversity_mip

import num_rules

class RuleFitPlusHIPAR(object) :

    def __init__(self, tree_size = 4, sample_fract = 'default', max_rules = 2000, memory_par = 0.01,
                 tree_generator = None, rfmode = 'regress', lin_trim_quantile = 0.025, lin_standardise = True,
                 exp_rand_tree_size = True, model_type = 'rl', Cs = None, cv = 3, random_state = None,
                 support_bias = 0.5, min_support=0.02, reg_method="lars", error_metric='rmse') :
        ## Create the ruleFit instance that will do the rule search
        self._ruleFit = RuleFit(tree_size, sample_fract, max_rules, memory_par, tree_generator, rfmode,
                                lin_trim_quantile, lin_standardise, exp_rand_tree_size, model_type, Cs, cv, random_state)
        self.support_bias = support_bias
        self.min_support = min_support
        self.reg_method = reg_method
        self.error_metric = error_metric
        self._models = None


    def _buildModel(self, ruleStr, X, y) :
        model = {}
        #print(ruleStr)
        subregion = X.query(ruleStr)
        subregion = subregion[list(subregion.select_dtypes(exclude=['object']))]


        if not isinstance(self.min_support, (int, numpy.integer)) :
            minSupport = max(2, int(self.min_support))
        else:
            minSupport = self.min_support

        if len(subregion) < minSupport :
            return None

        ## Support
        model[3] = len(subregion)
        ## The ids of the rows in the region
        model[7] = subregion.index
        y_x = y[subregion.index]
        ## Now time to calculate the error
        if self.reg_method == "lin" :
            reg = LinearRegression()
        elif self.reg_method == "lars" :
            reg = LassoLars(alpha=.1, fit_path=False)
        else:
            reg = OrthogonalMatchingPursuit()


        reg.fit(subregion, y_x)
        model[0] = ruleStr
        model[1] = reg
        if self.reg_method == "lars" :
            if self.error_metric == 'rmse' :
                model[5] = mean_squared_error(numpy.array(y_x), reg.predict(subregion))
            else:
                model[5] = median_absolute_error(numpy.array(y_x), reg.predict(subregion))
        else:
            if self.error_metric == 'rmse' :
                model[5] = mean_squared_error(y_x, reg.predict(subregion))
            else :
                model[5] = median_absolute_error(y_x, reg.predict(subregion))

        model[8] = False # For the moment we do not support polynomial models
        return model


    def _parseComplexQuery(self, cquery):
        # vendor_name=sperry <= 0.5 & PRP > 690.0
        conds = cquery.split(' & ')
        newConds = []
        for cond in conds:
            match = re.search('([-_a-zA-Z0-9]+=[-:/._a-zA-Z0-9\(\),\s]+)(\s[><]=?)(\s[01]\.[0-9]+)', cond)
            if match is not None:
                newCond = match.group(1)
                op = match.group(2)
                value = match.group(3)

                parts = newCond.split('=')
                if '>' in op:
                    newCond = (parts[0] + "==" + "'" + parts[1] + "'")
                else :
                    newCond = (parts[0] + '!=' + "'" + parts[1] + "'")

                newConds.append(newCond)
            else:
                newConds.append(cond)

        return " & ".join(newConds)

    def _prepareRulesForHipar(self, complexRules, oneAtomRules, X, y) :
        models = []
        ## Convert the rules to the HIPAR model and correct their support
        for index, rule in oneAtomRules.iterrows() :
            parts = rule['rule'].split('=')
            model = self._buildModel(parts[0] + '==' + "'" + parts[1] + "'", X, y)
            if model is None :
                continue

            models.append(model)

        for index, rule in complexRules.iterrows() :
            query = self._parseComplexQuery(rule['rule'])
            #print('query: ', query)
            model = self._buildModel(query, X, y)
            if model is None:
                continue

            models.append(model)

        return models


    def _cover_matrix(self, models, data):
        ndp = data.shape[0]
        ba = []

        for m in models:
            tba = pandas.Series(numpy.zeros(ndp, dtype=int), data.index)
            tba[m[7]] = 1
            ba.append(list(tba))

        ba = numpy.array(ba)
        # ba = csr_matrix(ba)
        return ba

    def getAvgNumberOfCoefficients(self):
        coefCounts = []
        for model in self._models :
            if model[0] != {} :
                coefCounts.append(len(model[1].coef_))

        return numpy.average(coefCounts) if len(coefCounts) > 0 else 0


    def fit(self, X, y):
        vec = DictVectorizer(sparse=False)
        _X_ = vec.fit_transform(X.to_dict('records'))
        _y_ = numpy.array(y)
        ## Learn the rules with RuleFit
        self._ruleFit.fit(_X_, _y_, vec.feature_names_)
        rules = self._ruleFit.get_rules()
        ## Bring those rules to the structure required by HIPAR
        oneAtomRules = rules[rules.type == 'linear']
        oneAtomRules = oneAtomRules[oneAtomRules['rule'].str.contains('=')]
        complexRules = rules[rules.type == 'rule']

        models = self._prepareRulesForHipar(complexRules, oneAtomRules, X, y)
        self._models = []

        if len(models) > 0 :
            ## Time to convert the rules to the HIPAR format
            belongs = self._cover_matrix(models, X)
            ## If we set this to 1, we should always get a solution
            chosen, solutions = diversity_mip(1, models, 0, cover_all=False,
                                              overlap_constraint='jaccard', support_bias=self.support_bias,
                                              strict_overlap=False, belongs=belongs, no_k=False, max_prediction=False)

            for c in chosen:
                self._models.append(models[c])

            not_covered = num_rules.not_covered_indexes(belongs, chosen)
            numattr = list(X.select_dtypes(include=['int64', 'float64']))
            X = X[numattr]
            defreg = LassoLars()
            if not_covered.size > 0:
                defreg.fit(X.iloc[not_covered], y.iloc[not_covered])
            else:
                defreg.fit(X, y)
        else :
            defreg = LassoLars()
            defreg.fit(X, y)

        # Add the default model
        self._models.append(({}, defreg, None, None, None, None, None, None, False))

        #print('The models', self._models)
        return self

    def predict(self, X):
        self._outStats = {}
        return num_rules.predict_with_boosting(self._models, X, [],
                                     X.select_dtypes(exclude=['object']).columns.tolist(),
                                     lambda x, y : x, self._outStats)




if __name__ == "__main__":
    cpu_types = {'Data': str, 'Ext': str}
    X_cpu = pandas.read_csv('../datasets/cpu.data', dtype=cpu_types)
    X_cpu.dropna(axis=0)
    cpu_target = 'ERP'
    y_cpu = X_cpu[cpu_target]
    del X_cpu[cpu_target]

    learner = RuleFitPlusHIPAR(max_rules=10)
    learner.fit(X_cpu, y_cpu)

    yp = learner.predict(X_cpu)
    print(mean_squared_error(yp, numpy.array(y_cpu)))

    X_yatch = pandas.read_csv('../datasets/yatch.csv')
    yatch_target = 'residuary_resistance'
    y_yatch =  X_yatch[yatch_target]
    del X_yatch[yatch_target]

    learner = RuleFitPlusHIPAR(max_rules=10)
    learner.fit(X_yatch, y_yatch)

    yp = learner.predict(X_yatch)
    print(mean_squared_error(yp, numpy.array(y_yatch)))
