import io
import pandas
import numpy


'''
Read an file containing pattern descriptions (in the SPMF format) and create a DataFrame from it
'''
def read_pattern_file(file):
    f = io.open(file, mode='r', encoding='utf-8')
    content = f.readlines()   
    
    futur_df = [] #list of dict
    attributes = [] #list of attributes in the patterns    
    
    for l in content:
        #Replace in order to be able to split on spaces
        content = l.replace("Parcelle Référence", "Parcelle_Reference")
        
        line_content = content.split(' ')
        tmpdict = {}
        
        for e in line_content:            
            tmp_elem = e.split('=')
            
            if len(tmp_elem) > 1:
                attr = tmp_elem[0]
                value = tmp_elem[1]
            
            if not (attr in attributes):
                attributes.append(attr)
            
            tmpdict[attr] = value
        
        supp = int(line_content[len(line_content)-1])
        tmpdict['support'] = supp
        
        futur_df.append(tmpdict)
    
    new_futur_df = []
    
    for p in futur_df:
        keys = p.keys()
        
        for a in attributes:
            if not (a in keys):
                p[a] = None
        
        new_futur_df.append(p)
    
    df = pandas.DataFrame(new_futur_df)
    
    return df



'''
Read an ARFF file and create a DataFrame from it
'''
def read_arff_to_dataframe(file):
    out = {}
    
    f = io.open(file, mode='r', encoding="utf-8")
    content = f.readlines()
    
    attributes = {} #We store the type of the attributes
    attributes_id = {} #We need to keep the order of the attributes in order to parse the data
    attr_num = 0
    
    data = False
    
    for l in content:       
        
        #Remove newline characters
        l = l.strip('\n')
        l = l.strip('\t')
        
        l = l.replace('Parcelle Référence', 'Parcelle_Reference') #Remove this later for more generic code
        
        if '@attribute' in l: #Construct the list of attributes
            s = l.split(' ')
            attr_name = s[1].replace('-', '_')
            attr_type = s[2]
            attributes[attr_name] = attr_type
            attributes_id[attr_name] = attr_num
            attr_num += 1
        elif '@data' in l: #Detect the begining of the data section
            data = True
            for a in attributes:
                out[a] = []
        
        if data:
            #tmpd = {}
            tmpl = l.split(',')
            
            if len(tmpl) == attr_num: #Test if the list is not empty
                for i in attributes.keys():
                    tmpval = tmpl[attributes_id[i]]
                    if tmpval != '?':
                        if attributes[i] == 'numeric':
                            tmpval = 0.0 if tmpval == '-0.0' else tmpval
                            out[i].append(numpy.float64(tmpval))
                        else:
                            out[i].append(str(tmpval))
                    else:
                        if attributes[i] == 'numeric':
                            out[i].append(numpy.nan)
                        else:
                            out[i].append('')

    df = pandas.DataFrame(out)
           
    return df