'''
Gradient boosting using HIPAR as base learner.
'''

import sys
import pandas
import numpy
import random
import math

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import median_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold

sys.path.append('../hipar_libs/')
from num_rules import mine_num_rules2
from num_rules import mine_num_rules_debug
from num_rules import predict_with_boosting
from num_rules import pattern_query
import naive_rules

class HIPARRegressor(object) :

    def __init__(self, min_support=0.02, support_bias=0.5, reg_type="omp", excluded_attrs=None,
                 excluded_conds=None, output_file='./hipar', error_metric='rmse', overlap_bias=1.0,
                 force_discretize=False, discretize=True, measure_time=False):
        self.min_support = min_support
        self.reg_type = reg_type
        self._models = None
        self._excluded_attrs = excluded_attrs
        self._excluded_conds = excluded_conds
        self.support_bias = support_bias
        self.output_file = output_file
        self.error_metric = error_metric
        self.overlap_bias = overlap_bias
        self.force_discretize = force_discretize
        self.discretize = discretize
        self.measure_time = measure_time
        self._train_stats = {}


    def fit(self, X, y):
        Xp = X.copy()
        Xp['target'] = y
        Xp = Xp.reset_index(drop=True)

        if not self.measure_time :
            self._models = mine_num_rules2(Xp, 'target', self.min_support, output_file=self.output_file, poly_reg=False,
                                           discretize=self.discretize,
                                           support_bias=self.support_bias, ignored_attrs=self._excluded_attrs,
                                           reg_type=self.reg_type, ignored_attrs_for_disc=None,
                                           ignored_sym_conds=self._excluded_conds, error_metric=self.error_metric,
                                           overlap_bias=self.overlap_bias, force_discretize=self.force_discretize,
                                           discretize_method='mdlp-tv')
        else:
            self._models, stats = mine_num_rules_debug(Xp, 'target', self.min_support, output_file=self.output_file,
                                                       poly_reg=False,
                                           discretize=self.discretize,
                                           support_bias=self.support_bias, ignored_attrs=self._excluded_attrs,
                                           reg_type=self.reg_type, ignored_attrs_for_disc=None,
                                           ignored_sym_conds=self._excluded_conds, error_metric=self.error_metric,
                                           overlap_bias=self.overlap_bias, force_discretize=self.force_discretize,
                                           discretize_method='mdlp-tv')
            self._train_stats.update(stats)

        return self

    def getUsedAttributes(self):
        attrs = set()

        for m in self._models :
            for key in m[0] :
                attrs.add(key)

        return attrs

    def getUsedConditions(self):
        conditions = set()
        for m in self._models :
            for key, value in m[0].items() :
                conditions.add((key, value))

        return conditions

    def predict(self, X):
        self._outStats = {}
        return predict_with_boosting(self._models, X, [],
                                               X.select_dtypes(exclude=['object']).columns.tolist(),
                                               pattern_query, self._outStats)




class GradientBoostingRules(object) :

    def __init__(self, min_support=0.02, support_bias=0.5, reg_type="omp", n_estimators=10, tol=0.0001) :
        self.min_support = min_support
        self.support_bias = support_bias
        self.reg_type = reg_type
        self.n_estimators = n_estimators
        self.tol = tol
        self._estimators = None
        self._alphas = [1.0]
        self._alphaVectors = [[1.0]]
        self._estimatorVectors = []


    def fit(self, X, y):
        Xp = X.reset_index(drop=True)
        self._estimators = []
        regressor0 = HIPARRegressor(min_support=self.min_support,
                                    support_bias=self.support_bias,
                                    reg_type=self.reg_type)
        regressor0.fit(Xp, y)
        self._estimators.append(regressor0)
        yp = regressor0.predict(Xp)
        ri_1 = yp
        ignoredAttrs = regressor0.getUsedAttributes()
        ignoredConds = regressor0.getUsedConditions()
        #ignoredConds = set()
        #ignoredAttrs = set()
        old_error = numpy.sum(numpy.abs(ri_1))
        for i in range(self.n_estimators - 1) :
            ri = y - ri_1
            regressori = HIPARRegressor(min_support=self.min_support,
                                        support_bias=self.support_bias,
                                        reg_type=self.reg_type,
                                        excluded_attrs=list(ignoredAttrs),
                                        excluded_conds=list(ignoredConds))
            #print('Ignored conds', ignoredConds)
            regressori.fit(Xp, ri)
            hi = regressori.predict(Xp)


            df = pandas.DataFrame({'r' : hi}, index=Xp.index)

            lRLearner = LinearRegression(fit_intercept=False)
            lRLearner.fit(df, y - ri_1)
            self._alphas.append(lRLearner.coef_[0])

            ri_1 = ri_1 + (lRLearner.coef_[0] * hi)
            self._estimators.append(regressori)

            ignoredAttrs.update(regressori.getUsedAttributes())
            ignoredConds.update(regressori.getUsedConditions())
            error = numpy.sum(numpy.abs(ri))

            if len(regressori._models) == 1 :
                break
            #if old_error >= error and abs(old_error - error) <= self.tol:
            #    print('Tolerance surpassed')
            #    break

            old_error = error

        return self

    def _difference(self, y, matrix) :
        newMatrix = []
        for yp in matrix :
            delta = numpy.full(y.shape[0], numpy.inf)
            for idx in range(y.shape[0]) :
                if yp[idx] != numpy.inf :
                    delta[idx] = y[idx] - yp[idx]

            newMatrix.append(delta)

        return newMatrix

    def fit2(self, X, y):
        Xp = X.reset_index(drop=True)
        regressor0 = HIPARRegressor(min_support=self.min_support,
                                    support_bias=self.support_bias,
                                    reg_type=self.reg_type)
        regressor0.fit(Xp, y)
        self._estimatorVectors.append([regressor0])
        yp = regressor0.predict(Xp)
        YpRaw = regressor0._outStats['individual_predictions']
        ri_1 = yp
        ri_1Raw = YpRaw
        ignoredAttrs = regressor0.getUsedAttributes()
        ignoredConds = regressor0.getUsedConditions()

        for i in range(self.n_estimators) :
            alphasForRound = []
            regressorForRound = []
            ignoredAttrsPerRound = set()
            ignoredCondsPerRound = set()
            ri = y - ri_1
            error = numpy.sum(numpy.abs(ri))

            if error < self.tol :
                break

            riRaw = self._difference(y, ri_1Raw)
            nextRaw = []
            for j in range(len(riRaw)) :
                rij = riRaw[j].copy()
                mask = (rij != numpy.inf)
                if not numpy.any(mask) :
                    continue

                # Do not boost this rule
                if numpy.sum(rij) < self.tol :
                    continue

                rij = rij[mask]
                rij_1 = ri_1Raw[j][mask]
                regressorij = HIPARRegressor(min_support=self.min_support,
                                            support_bias=self.support_bias,
                                            reg_type=self.reg_type,
                                            excluded_attrs=list(ignoredAttrs),
                                            excluded_conds=list(ignoredConds))
                Xpl = Xp.iloc[mask].reset_index(drop=True)

                regressorij.fit(Xpl, rij)
                hij = regressorij.predict(Xpl)

                df = pandas.DataFrame({'r' : hij})
                lRLearner = LinearRegression(fit_intercept=False)
                ymask = y[mask].reset_index(drop=True)

                #print(ymask)
                lRLearner.fit(df, ymask - rij_1)

                alphasForRound.append(lRLearner.coef_[0])
                rij_1 = rij_1 + (lRLearner.coef_[0] * hij)
                regressorForRound.append(regressorij)

                ignoredAttrsPerRound.update(regressorij.getUsedAttributes())
                ignoredCondsPerRound.update(regressorij.getUsedConditions())

                ri_1[mask] = rij_1
                rij_1Next = numpy.zeros(ri_1.shape[0])
                rij_1Next[mask] = rij_1
                nextRaw.append(rij_1Next)

            ri_1Raw = nextRaw

            self._alphaVectors.append(alphasForRound)
            self._estimatorVectors.append(regressorForRound)


            ignoredAttrs.update(ignoredAttrsPerRound)
            ignoredConds.update(ignoredCondsPerRound)
            #if old_error >= error and abs(old_error - error) <= self.tol:
            #    print('Tolerance surpassed')
            #    break
            #print('Error', error)
            #if error < self.tol :
            #    break



        return self

    def predict(self, X):
        yp = numpy.zeros(len(X))
        for i in range(len(self._estimators)) :
            yp = yp + self._alphas[i] * self._estimators[i].predict(X)

        return yp

    def predict2(self, X):
        yp = numpy.zeros(len(X))
        for i in range(len(self._estimatorVectors)) :
            for j in range(len(self._estimatorVectors[i])) :
                #print(yp)
                yp = yp + self._alphaVectors[i][j] * self._estimatorVectors[i][j].predict(X)


        return yp

if __name__ == "__main__":
    import warnings
    warnings.filterwarnings("ignore")
    if len(sys.argv) < 3:
        print('Not enough arguments')
        print('gradient_boosting_rules.py [CSV dataset] [target variable]')
        sys.exit(1)

    X_cpu = pandas.read_csv(sys.argv[1])
    cpu_target = sys.argv[2]

    X_cpu = X_cpu.dropna(axis=0).reset_index(drop=True)
    y_cpu = X_cpu[cpu_target]
    del X_cpu[cpu_target]

    X_train, X_test, y_train, y_test = train_test_split(X_cpu, y_cpu, test_size=0.1)

    kf = KFold(n_splits=5)
    bestError = math.inf
    bestModel = None
    error_metric = 'rmse'
    rmses = []
    maes = []
    for tr, ts in kf.split(X_train):
        print('Learning model')
        learner = HIPARRegressor(min_support=0.02, support_bias=1.0, overlap_bias=1.0, reg_type="dynamic",
                                 force_discretize=False, error_metric=error_metric, excluded_attrs=[])
        y_traink = y_train.iloc[tr]
        X_traink = X_train.iloc[tr]
        learner.fit(X_traink, y_traink)
        print('Model learnt')
        print('Model')
        print(naive_rules.pretty_rules(learner._models, list(X_train.select_dtypes(include=['int64', 'float64'])),
                                       [], cpu_target))
        X_testk = X_train.iloc[tr].reset_index(drop=True)
        y_testk = y_train.iloc[tr].reset_index(drop=True)
        yp = learner.predict(X_testk)
        rmse = math.sqrt(mean_squared_error(yp, numpy.array(y_testk)))
        meae = median_absolute_error(yp, numpy.array(y_testk))
        if error_metric == 'mae' :
            if meae < bestError :
                bestError = meae
                bestModel = learner
        else:
            if rmse < bestError :
                bestError = rmse
                bestModel = learner

        print('RMSE: ', rmse)
        print('MAE: ', mean_absolute_error(yp, numpy.array(y_testk)))
        print('MeAE: ', meae)
        rmses.append(rmse)
        maes.append(meae)
        print('End model')


    X_test = X_test.reset_index(drop=True)
    yp = bestModel.predict(X_test)
    print('Best model')
    print(naive_rules.pretty_rules(bestModel._models,
                                   list(X_train.select_dtypes(include=['int64', 'float64'])),
                                                           [], cpu_target))
    print('RMSE: ', math.sqrt(mean_squared_error(yp, numpy.array(y_test))))
    print('MAE: ', mean_absolute_error(yp, numpy.array(y_test)))
    print('MeAE: ', median_absolute_error(yp, numpy.array(y_test)))
    print('---------')

    print('Median MeAE: ', numpy.median(maes))
    print('Average RMSE: ', numpy.mean(rmses))