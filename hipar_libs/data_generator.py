import random
import copy
import pandas
import math
import numpy
import sys

from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import median_absolute_error
from sklearn.metrics import r2_score
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction import DictVectorizer
from scipy.stats import gmean
from scipy.stats import hmean
import matplotlib.pyplot as plt
from datetime import datetime



sys.path.append('../hipar_libs/')
from num_rules import mine_num_rules2
from num_rules import predict_with_boosting
from num_rules import pattern_query
import naive_rules
from gradient_boosting_rules import HIPARRegressor

sys.path.append('../../LearningX/advanced_ML/model_tree/src')
from ModelTree import ModelTree

sys.path.append('../../LearningX/advanced_ML/model_tree/models')
from linear_regr import linear_regr

sys.path.append('../regression/')
from lars_regression import lars_regression


def gen_symb_vals(all_symb_attr, maxvals):
    vals = {}
    k = 0
    for symb_name in all_symb_attr:
        vals[symb_name] = []
        for value in range(maxvals[k]) :
            v = symb_name + '_val' + str(value)
            vals[symb_name].append(v)
        k = k + 1
    return vals


def gen_num_vals(id_law, nlaws):
    minv = -100 * (nlaws) + (id_law + 1) * 100
    maxv = minv + 100
    return (minv, maxv)


def gen_symb_attrs(nsymb, nlaws):
    s = []
    maxvals = None
    if nsymb == 1 :
        maxvals = (nlaws, )
    else :
        for j in range(2, 6) :
            if nlaws % j == 0 :
                maxvals = (j, int(nlaws / j))
                break
        if maxvals is None :
            maxvals = (nlaws, 1)

    for i in range(nsymb):
        va = 'a_s' + str(i)
        s.append(va)

    vals = gen_symb_vals(s, maxvals)
    return s, vals


def gen_num_attrs(nnum):
    s = []
    for i in range(nnum):
        s.append('a_n' + str(i))
    return s


def gen_numlaw(num_attrs, id_law):
    coefs_vec = []
    coefs_dict = {}
    intercept = 0.0
    for i in range(len(num_attrs) + 1):
        sign = 1 if (id_law >> i) & 1 == 0 else -1
        if i == len(num_attrs) :
            intercept = random.random() * sign
        else :
            coef = random.random() * sign
            coefs_dict[num_attrs[i]] = coef
            coefs_vec.append(coef)

    return coefs_dict, coefs_vec, intercept


def gen_subdata(nsamples, symb_attrs, symb_vals, all_combinations, num_attrs, noise, id_law):
    law = {}
    data = []
    if len(symb_attrs) == 1 :
        law['pattern'] = { symb_attrs[0] : symb_vals[symb_attrs[0]][id_law] }
    else:
        law['pattern'] = {}
        id_attr = 0
        for comb in all_combinations[id_law] :
            law['pattern'][symb_attrs[id_attr]] = comb
            id_attr += 1

    
    coefs, coefs_vec, intercept = gen_numlaw(num_attrs, id_law)
    law['coefs'] = coefs
    law['coefs_vec'] = coefs_vec
    law['intercept'] = intercept
    nvals = {}

    for j in range(len(num_attrs)):
        a = num_attrs[j]
        nvals[a] = gen_num_vals(j, len(num_attrs))
        law[a] = (nvals[a], coefs[a])
        
    for r in range(nsamples):
        row = {}
        for a in symb_attrs:
            row[a] = law['pattern'][a]

        targetval = intercept
        for a in num_attrs:
            tmp = random.uniform(nvals[a][0], nvals[a][1])
            row[a] = tmp
            targetval += coefs[a] * tmp


        row['target'] = targetval + random.gauss(0,noise)*targetval
        data.append(row)
        
    return data, law


def partition(m, n, vals):
    if n == 1:
        vals.append(m)
        random.shuffle(vals)
        return
    mv = int(m / 2)
    s = random.randint(int(mv/2), mv)
    vals.append(s)
    partition(m-s, n-1, minsize, vals)
    
    
def partition2(law_min_size, law_max_size, nrules):
    parts = []
    for nn in range(nrules):
        s = random.randint(law_min_size, law_max_size)
        parts.append(s)
        
    return parts

def computeAllCombinations(attrs2values) :
    if len(attrs2values) == 1 :
        return list(attrs2values.items())
    else :
        combinations = []
        keys = list(attrs2values.keys())
        for i in range(len(attrs2values[keys[0]])) :
            for j in range(len(attrs2values[keys[1]])) :
                combinations.append((attrs2values[keys[0]][i], attrs2values[keys[1]][j]))

        return combinations


'''
Generate an artificial dataset composed of several laws

Keyword arguments:

nsamples -- the total number of samples to generate
nnum -- the number of numeric attributes (can't be less than 1)
nlaws -- the number of different laws used to generate the dataset
noise -- the standard deviation of the gaussian noise (0 = no noise)
seed -- the seed used for the random generator
'''
def gen_dataset(law_min_size, law_max_size, nsymb, nnum, nlaws, noise, seed=42, print_infos=True):
    
    if (nsymb < 1 or nsymb > 2):
        nsymb = 1
        print('Warning : nsymb must be either 1 or 2')
        print('nsymb set to 1')
    
    if (nnum < 1):
        nnum = 1
        print('Warning : nnum < 1')
        print('nsymb set to 1')
    
    random.seed(seed)
        
    #parts = []
    #partition(nsamples, nlaws, parts)
    parts = partition2(law_min_size, law_max_size, nlaws)
    data_size = sum(parts)
    
    if print_infos:
        print('Dataset size : ' + str(data_size))
        print('Laws size : ' + str(parts))
        print('Support of the smallest law : ' + str(round(min(parts)/data_size,2)*100) + '%\n')
    
    dataset_size = sum(parts)
    
    data = []
    
    symb_attrs, symb_vals = gen_symb_attrs(nsymb, nlaws)
    all_combinations = computeAllCombinations(symb_vals)

    all_symb_vals = copy.deepcopy(symb_vals)
    
    num_attrs = gen_num_attrs(nnum)
    
    laws = []
    law_id = 0
    for n in parts:
        sdata, law = gen_subdata(n, symb_attrs, symb_vals, all_combinations,
                                                 num_attrs, noise, law_id)
        law_id = law_id + 1
        indexes = list(range(len(data), (len(data)+len(sdata))))
        law['indexes'] = indexes
        data = data + sdata
        laws.append(law)
        
    df = pandas.DataFrame(data)
    
    df = df.sample(frac=1).reset_index(drop=False)
    
    for l in laws:
        new_indexes = list(df[df['index'].isin(l['indexes'])].index)
        l['indexes'] = new_indexes #this is not a copy, so it replace in the main 'laws' list
    
    df = df.drop('index', axis=1)
    return df, laws, parts

def float_jaccard(v1, v2) :
    summin = 0.0
    summax = 0.0

    for i in range(len(v1)) :
        summin += min(v1[0][i], v2[0][i])
        summax += max(v1[0][i], v2[0][i])

    return summin / summax if summax > 0 else 0.0

def evaluate_precision_and_recall_for_single_law(models, law, data, model_trees=False, fix_symb_attrs=False) :
    precisions = []
    recalls = []
    f1s = []
    cosines = []
    f1_cosine = []
    jaccards = []
    full_jaccards = []

    for model in models :
        if model[0] == {} :
            continue

        if model_trees and fix_symb_attrs :
            patternsModel = dict(model[0])
            for key, val in model[0].items() :
                if val.startswith('na_') :
                    trueVal = val[1:len(val)]
                    newVal = None
                    values4attribute = data[key].unique()
                    for value4attr in values4attribute :
                        if value4attr != trueVal :
                            newVal = value4attr
                    patternsModel[key] = newVal
        else:
            patternsModel = model[0]

        lawModel = law['pattern']
        lawCoefVector = list(law['coefs_vec'])
        lawCoefVector.append(law['intercept'])

        if model_trees:
            patternsModelVector = list(model[2])
        else:
            if len(model[1].coef_.shape) > 1:
                modelCoefs = model[1].coef_.ravel()
            else :
                modelCoefs = model[1].coef_

            patternsModelVector = list(modelCoefs)

        if type(model[1].intercept_) == numpy.array :
            patternsModelVector.append(model[1].intercept_[0])
        else :
            patternsModelVector.append(model[1].intercept_)

        patternsModelVector = numpy.array(patternsModelVector).reshape(1, -1)
        lawCoefVector = numpy.array(lawCoefVector).reshape(1, -1)
        #print('LawModel',  lawModel)
        #print('Pattern Model', patternsModel)
        #print(lawCoefVector, patternsModelVector)
        cosine = cosine_similarity(lawCoefVector, patternsModelVector)
        fjaccard = float_jaccard(lawCoefVector, patternsModelVector)
        setmodel = set(patternsModel.items())
        setlaw = set(lawModel.items())
        intersection = setlaw & setmodel
        precision = len(intersection) / len(setmodel)
        recall = len(intersection) / len(setlaw)
        jaccard = len(intersection) / len(setlaw | setmodel)
        f1 = hmean([precision, recall]) if precision > 0.0 or recall > 0.0 else 0.0
        precisions.append(precision)
        recalls.append(recall)
        f1s.append(f1)
        jaccards.append(jaccard)
        cosineVal = (cosine[0][0] + 1)/2
        cosines.append(cosineVal)
        f1_cosine.append(numpy.mean([fjaccard, cosineVal, f1]) if precision > 0.0 and recall > 0.0 and cosineVal > 0.0 else 0.0)
        full_jaccards.append(numpy.mean([jaccard, fjaccard, cosineVal]) if jaccard > 0.0 and recall > 0.0 and cosineVal > 0.0 else 0.0)


    return precisions, recalls, f1s, cosines, f1_cosine, jaccards, full_jaccards

def evaluate_precision_and_recall(models, laws, data, model_trees=False, fix_symb_attrs=False) :
    precisions = []
    recalls = []
    f1s = []
    cosines = []
    fullscores = []
    jaccards = []
    full_jaccards = []
    f1_jaccard = []
    jaccard_cosine = []

    for law in laws :
        precisionsLaw, recallsLaw, f1sLaw, cosineLaw, fullscoreLaw, jaccardLaw, full_jaccardLaw = \
            evaluate_precision_and_recall_for_single_law(models, law, data, model_trees, fix_symb_attrs)

        maxPrecision = numpy.max(precisionsLaw) if len(precisionsLaw) > 0 else 0.0
        maxRecall = numpy.max(recallsLaw) if len(recallsLaw) > 0 else 0.0
        maxF1 = numpy.max(f1sLaw) if len(recallsLaw) > 0 else 0.0
        cosines.append(numpy.max(cosineLaw) if len(cosineLaw) > 0 else 0.0)
        jaccards.append(numpy.max(jaccardLaw) if len(jaccardLaw) > 0 else 0.0) # Add the max
        full_jaccards.append(numpy.max(full_jaccardLaw) if len(full_jaccardLaw) > 0 else 0.0) #Add the max
        precisions.append(maxPrecision)
        recalls.append(maxRecall)
        f1s.append(maxF1)
        fullscores.append(numpy.max(fullscoreLaw) if len(fullscoreLaw) > 0 else 0.0)

    return numpy.mean(precisions), numpy.mean(recalls), numpy.mean(f1s), numpy.mean(cosines), numpy.mean(fullscores), \
           numpy.mean(jaccards), numpy.mean(full_jaccards)


def compactStack(stack) :
    intervalsDict = {}
    newStack = []
    for tp in stack :
        if tp[0] not in intervalsDict :
            intervalsDict[tp[0]] = {'<=' : [], '>' : [], '=' : []}

        if tp[1].startswith('<=') :
            intervalsDict[tp[0]]['<='].append(float(tp[1].replace('<=', '')))
        elif tp[1].startswith('>') :
            intervalsDict[tp[0]]['>'].append(float(tp[1].replace('>', '')))
        else :
            intervalsDict[tp[0]]['='].append(tp[1])

    for attribute, valDict in intervalsDict.items() :
        lowerbound = float('-inf')
        upperbound = float('inf')
        finalVal = None
        if len(valDict['<=']) > 0 :
            upperbound = min(valDict['<='])
        if len(valDict['>']) > 0 :
            lowerbound = max(valDict['>'])
        if len(valDict['=']) > 0 :
            finalVal = valDict['='][0]

        if finalVal is None :
            finalVal = ''
            if lowerbound > float('-inf') :
                finalVal = str(lowerbound) + '<'
            finalVal += attribute
            if upperbound < float('inf') :
                finalVal += '<=' + str(upperbound)

        newStack.append((attribute, finalVal))

    return newStack

def traverse_model_tree(node, vocabulary, inv_vocabulary, results, stack, polarity) :
    if not polarity:
        if '<=' in stack[len(stack) - 1][1]:
            stack[len(stack) - 1] = (stack[len(stack) - 1][0], stack[len(stack) - 1][1].replace('<=', '>'))
        else:
            stack[len(stack) - 1] = (stack[len(stack) - 1][0], 'n' + stack[len(stack) - 1][1])

    ## See from where this tree comes from
    no_children = node["children"]["left"] is None and node["children"]["right"] is None
    if no_children :
        ## Make a rule, add it to results, and remove the last element from the stack
        newCoefs = numpy.zeros(sum(1 for v in vocabulary if '_n' in v))
        indexes = []
        for k, v in vocabulary.items() :
            if '_n' in k :
                indexes.append(int(v))

        indexes = sorted(indexes)
        coefsLastIdx = len(indexes)
        for i in range(coefsLastIdx) :
            if len(node['model'].model.coef_.shape) > 1 :
                newCoefs[i] = node['model'].model.coef_[0][indexes[i]]
            else :
                newCoefs[i] = node['model'].model.coef_[indexes[i]]


        #if type(node['model'].model.intercept_) == numpy.array :
        #   if len(node['model'].model.intercept_.shape) > 1 :
        #       newCoefs[coefsLastIdx] = node['model'].model.intercept_[0][0]
        #   else :
        #       newCoefs[coefsLastIdx] = node['model'].model.intercept_[0]
        #else :
        #    newCoefs[coefsLastIdx] = node['model'].model.intercept_

        ## Here, pay attention!
        newStack = compactStack(stack)
        results.append(({c[0] : c[1] for c in newStack}, node['model'].model, newCoefs))
    else :
        condition = inv_vocabulary[node["j_feature"]]

        if '=' in condition:
            parts = condition.split('=')
            stack.append((parts[0], parts[1]))
        else :
            stack.append((condition, '<=' + str(node['threshold'])))

        traverse_model_tree(node['children']['left'], vocabulary, inv_vocabulary, results, list(stack), True)
        traverse_model_tree(node['children']['right'], vocabulary, inv_vocabulary, results, list(stack), False)


def modelTree2HiparModel(modelTree, vocabulary) :
    inv_vocabulary = {v: k for k, v in vocabulary.items()}
    results = []
    stack = []
    traverse_model_tree(modelTree.tree, vocabulary, inv_vocabulary, results, stack, True)
    #print(results)
    return results

def evaluate_artificial_dataset(X_cpu, laws, min_support=0.02, verbose=True, error_metric='rmse', fix_symb_attrs=False,
                                ntimes=1, small_mts=False, exp_type='simple') :
    import warnings
    warnings.filterwarnings("ignore")
    y_cpu = X_cpu['target']
    del X_cpu['target']

    bestError = math.inf
    bestModel = None
    rmses = {'hipar' : [], 'mt' : [], 'mtn': []}
    maes = {'hipar' : [], 'mt' : [], 'mtn': []}
    precisions = {'hipar' : [], 'mt' : [], 'mtn': []}
    recalls = {'hipar' : [], 'mt' : [], 'mtn': []}
    f1s = {'hipar' : [], 'mt' : [], 'mtn': []}
    cosines = {'hipar' : [], 'mt' : [], 'mtn': []}
    r2s = {'hipar' : [], 'mt' : [], 'mtn': []}
    fullscores = {'hipar' : [], 'mt' : [], 'mtn': []}
    n_rules = {'hipar' : [], 'mt' : [], 'mtn': []}
    jaccards = {'hipar' : [], 'mt' : [], 'mtn': []}
    full_jaccards = {'hipar' : [], 'mt' : [], 'mtn': []}

    for i in range(ntimes) :
        X_train, X_test, y_train, y_test = train_test_split(X_cpu, y_cpu, test_size=0.1)
        kf = KFold(n_splits=10)

        for tr, ts in kf.split(X_train):
            if exp_type == 'simple' :
                learner = HIPARRegressor(min_support=min_support, support_bias=0.5, overlap_bias=1.0, reg_type="lin",
                                         force_discretize=False, error_metric=error_metric, excluded_attrs=[],
                                         discretize=False)
            elif exp_type == 'full' :
                learner = HIPARRegressor(min_support=min_support, support_bias=0.5, overlap_bias=1.0, reg_type="lin",
                                         force_discretize=False, error_metric=error_metric, excluded_attrs=[],
                                         discretize=False)
            else :
                learner = HIPARRegressor(min_support=min_support, support_bias=1.0, overlap_bias=1.0, reg_type="lin",
                                         force_discretize=True, error_metric=error_metric, excluded_attrs=[],
                                         discretize=True)

            y_traink = y_train.iloc[tr]
            X_traink = X_train.iloc[tr]
            learner.fit(X_traink, y_traink)
            #print('Models')
            #for model in learner._models :
            #    print(model[0])
            #print('End of models')

            mtLearnerN = None
            mtRulesN = None

            if small_mts :
                mtlearnerN = ModelTree(linear_regr(), min_samples_leaf=min_support, max_depth=max(1, math.floor(
                    math.log(len(learner._models), 2))))

            mtlearner = ModelTree(linear_regr(), min_samples_leaf=min_support)
            dico = X_traink.to_dict('records')
            vec = DictVectorizer(sparse=False)
            X_traink_vec = vec.fit_transform(dico)
            y_traink_vec = numpy.array(y_traink)
            mtlearner.fit(X_traink_vec, y_traink_vec)

            #print(learner._models)
            if verbose :
                print('Model learnt')
                print(naive_rules.pretty_rules(learner._models, list(X_train.select_dtypes(include=['int64', 'float64'])),
                                               [], 'target'))
            X_testk = X_train.iloc[tr].reset_index(drop=True)
            y_testk = y_train.iloc[tr].reset_index(drop=True)
            X_testk_vec = vec.transform(X_testk.to_dict('records'))
            yp = learner.predict(X_testk)
            ypMt = mtlearner.predict(X_testk_vec)

            y_testk_array = numpy.array(y_testk)

            if small_mts :
                mtlearnerN.fit(X_traink_vec, y_traink_vec)
                ypMtN = mtlearnerN.predict(X_testk_vec)

                rmseMtN = math.sqrt(mean_squared_error(ypMtN, numpy.array(y_testk)))
                meaeMtN = median_absolute_error(ypMtN, numpy.array(y_testk))
                r2MtN = r2_score(ypMt, y_testk)
                rmses['mtn'].append(rmseMtN)
                maes['mtn'].append(meaeMtN)
                r2s['mtn'].append(r2MtN)
                mtRulesN = modelTree2HiparModel(mtlearnerN, vec.vocabulary_)

            rmse = math.sqrt(mean_squared_error(yp, y_testk_array))
            meae = median_absolute_error(yp, y_testk_array)
            r2 = r2_score(yp, y_testk)
            rmses['hipar'].append(rmse)
            maes['hipar'].append(meae)
            r2s['hipar'].append(r2)

            rmseMt = math.sqrt(mean_squared_error(ypMt, numpy.array(y_testk)))
            meaeMt = median_absolute_error(ypMt, numpy.array(y_testk))
            r2Mt = r2_score(ypMt, y_testk)
            rmses['mt'].append(rmseMt)
            maes['mt'].append(meaeMt)
            r2s['mt'].append(r2Mt)

            mtRules = modelTree2HiparModel(mtlearner, vec.vocabulary_)
            #print('Model tree')
            #for mtr in mtRules :
            #    print(mtr)
            #print('end of model tree')
            precision, recall, f1, cosine, fullscore, jaccard, full_jaccard = \
                evaluate_precision_and_recall(learner._models, laws, X_traink)
            precisionMt, recallMt, f1Mt, cosineMt, fullscoreMt, jaccardMt, full_jaccardMt = \
                evaluate_precision_and_recall(mtRules, laws, X_traink, model_trees=True, fix_symb_attrs=fix_symb_attrs)

            if small_mts :
                precisionMtN, recallMtN, f1MtN, cosineMtN, fullscoreMtN, jaccardMtN, full_jaccardMtN = \
                    evaluate_precision_and_recall(mtRulesN, laws, X_traink, model_trees=True,
                                                  fix_symb_attrs=fix_symb_attrs)
                precisions['mtn'].append(precisionMtN)
                recalls['mtn'].append(recallMtN)
                f1s['mtn'].append(f1MtN)


                cosines['mtn'].append(cosineMtN)
                fullscores['mtn'].append(fullscoreMtN)
                n_rules['mtn'].append(len(mtRulesN))
                jaccards['mtn'].append(jaccardMtN)
                full_jaccards['mtn'].append(full_jaccardMtN)

            precisions['hipar'].append(precision)
            recalls['hipar'].append(recall)
            f1s['hipar'].append(f1)

            cosines['hipar'].append(cosine)
            fullscores['hipar'].append(fullscore)
            n_rules['hipar'].append(len(learner._models))
            jaccards['hipar'].append(jaccard)
            full_jaccards['hipar'].append(full_jaccard)

            precisions['mt'].append(precisionMt)
            recalls['mt'].append(recallMt)
            f1s['mt'].append(f1Mt)

            cosines['mt'].append(cosineMt)
            fullscores['mt'].append(fullscoreMt)
            n_rules['mt'].append(len(mtRules))
            jaccards['mt'].append(jaccardMt)
            full_jaccards['mt'].append(full_jaccardMt)


            if error_metric == 'mae':
                if meae < bestError:
                    bestError = meae
                    bestModel = learner
            else:
                if rmse < bestError:
                    bestError = rmse
                    bestModel = learner

            if verbose:
                print('RMSE: ', rmse)
                print('MAE: ', mean_absolute_error(yp, numpy.array(y_testk)))
                print('MeAE: ', meae)

            if verbose:
                print('End model')

        X_test = X_test.reset_index(drop=True)
        yp = bestModel.predict(X_test)
        if verbose:
            print('Best model')
            print(naive_rules.pretty_rules(bestModel._models,
                                           list(X_train.select_dtypes(include=['int64', 'float64'])),
                                           [], 'target'))
            print('RMSE: ', math.sqrt(mean_squared_error(yp, numpy.array(y_test))))
            print('MAE: ', mean_absolute_error(yp, numpy.array(y_test)))
            print('MeAE: ', median_absolute_error(yp, numpy.array(y_test)))
            print('---------')

            print('Median MeAE: ', numpy.median(maes))
            print('Average RMSE: ', numpy.mean(rmses))

    return rmses, maes, precisions, recalls, f1s, r2s, cosines, fullscores, n_rules, jaccards, \
           full_jaccards

def experimentsFull(small_mts=False, ntimes=3) :
    error_metric = 'rmse'
    noises = [round(x, 1) for x in numpy.arange(0.0, 1.1, 0.1)]
    noisesFullScoreDict = {}
    noisesRMSEDict = {}
    nRulesDict = {}
    fullJaccardDict = {}

    noisesFullScoreDictMt = {}
    noisesRMSEDictMt = {}
    nRulesDictMt = {}
    fullJaccardDictMt = {}

    noisesFullScoreDictMtN = {}
    noisesRMSEDictMtN = {}
    nRulesDictMtN = {}
    fullJaccardDictMtN = {}

    the_seed = datetime.now()

    for noise in noises :
        noisesFullScoreDict[noise] = ([], [])
        noisesRMSEDict[noise] = ([], [])
        nRulesDict[noise] = ([], [])
        fullJaccardDict[noise] = ([], [])

        noisesFullScoreDictMt[noise] = ([], [])
        noisesRMSEDictMt[noise] = ([], [])
        nRulesDictMt[noise] = ([], [])
        fullJaccardDictMt[noise] = ([], [])

        if small_mts :
            noisesFullScoreDictMtN[noise] = ([], [])
            noisesRMSEDictMtN[noise] = ([], [])
            nRulesDictMtN[noise] = ([], [])
            fullJaccardDictMtN[noise] = ([], [])

    for nlaws in range(2, 11) :
        fullscores2plot = []
        rmseplot = []
        nrulesplot = []
        full_jaccardplot = []

        fullscores2plotMt = []
        rmseplotMt = []
        nrulesplotMt = []
        full_jaccardplotMt = []

        if small_mts:
            fullscores2plotMtN = []
            rmseplotMtN = []
            nrulesplotMtN = []
            full_jaccardplotMtN = []

        for noise in noises :
            # def gen_dataset(law_min_size, law_max_size, nsymb, nnum, nlaws, noise, seed=42, print_infos=True):
            df, laws, lawsizes = gen_dataset(30, 30, 2, 3, nlaws, noise, seed=the_seed)
            #print(df)
            #for law in laws :
            #    print(law)
            #print(laws)
            maximum = numpy.max(df['target'])
            minimum = numpy.min(df['target'])
            variableRange = maximum - minimum
            min_support = min(lawsizes) / (len(df) * 2)
            rmses, maes, precisions, recalls, f1s, r2s, cosines, fullscores, n_rules, jaccards, \
            full_jaccards = evaluate_artificial_dataset(df, laws, min_support=min_support,
                                                                                     verbose=False, error_metric=error_metric,
                                                        small_mts=small_mts, ntimes=ntimes, exp_type='full')
            fullscore = numpy.mean(fullscores['hipar'])
            avgRules = numpy.mean(n_rules['hipar'])
            rmse = numpy.median(rmses['hipar'])
            avgfull_jaccard = numpy.mean(full_jaccards['hipar'])

            fullscoreMt = numpy.mean(fullscores['mt'])
            avgRulesMt = numpy.mean(n_rules['mt'])
            rmseMt = numpy.median(rmses['mt'])
            avgfull_jaccardMt = numpy.mean(full_jaccards['mt'])

            if small_mts :
                fullscoreMtN = numpy.mean(fullscores['mtn'])
                avgRulesMtN = numpy.mean(n_rules['mtn'])
                rmseMtN = numpy.median(rmses['mtn'])
                avgfull_jaccardMtN = numpy.mean(full_jaccards['mtn'])

            print('HIPAR: nlaws=', nlaws, 'noise=', noise, 'rmse=', rmse, 'mae=', numpy.mean(maes['hipar']),
                  'precision=', numpy.mean(precisions['hipar']), 'recall=', numpy.mean(recalls['hipar']), 'f1=',
                  numpy.mean(f1s['hipar']),
                  'r2=', numpy.mean(r2s['hipar']), 'cosine=', numpy.mean(cosines['hipar']), 'fs=', fullscore,
                  'nr=', avgRules, 'jacc=', numpy.mean(jaccards['hipar']),
                  'fulljacc=', avgfull_jaccard)

            print('MTs nlaws=', nlaws, 'noise=', noise, 'rmse=', rmseMt, 'mae=', numpy.mean(maes['mt']),
                  'precision=', numpy.mean(precisions['mt']), 'recall=', numpy.mean(recalls['mt']), 'f1=',
                  numpy.mean(f1s['mt']),
                  'r2=', numpy.mean(r2s['mt']), 'cosine=', numpy.mean(cosines['mt']), 'fs=', fullscoreMt,
                  'nr=', avgRulesMt, 'jacc=', numpy.mean(jaccards['mt']),
                  'fulljacc=', avgfull_jaccardMt)

            if small_mts :
                print('MTs(n) nlaws=', nlaws, 'noise=', noise, 'rmse=', rmseMtN, 'mae=', numpy.mean(maes['mtn']),
                      'precision=', numpy.mean(precisions['mtn']), 'recall=', numpy.mean(recalls['mtn']), 'f1=',
                      numpy.mean(f1s['mtn']),
                      'r2=', numpy.mean(r2s['mtn']), 'cosine=', numpy.mean(cosines['mtn']), 'fs=', fullscoreMtN,
                      'nr=', avgRulesMtN, 'jacc=', numpy.mean(jaccards['mtn']),
                      'fulljacc=', avgfull_jaccardMtN)

            fullscores2plot.append(fullscore)
            rmseplot.append(rmse / variableRange)
            nrulesplot.append(avgRules)
            full_jaccardplot.append(avgfull_jaccard)

            fullscores2plotMt.append(fullscoreMt)
            rmseplotMt.append(rmseMt / variableRange)
            nrulesplotMt.append(avgRulesMt)
            full_jaccardplotMt.append(avgfull_jaccardMt)

            if small_mts :
                fullscores2plotMtN.append(fullscoreMtN)
                rmseplotMtN.append(rmseMtN / variableRange)
                nrulesplotMtN.append(avgRulesMtN)
                full_jaccardplotMtN.append(avgfull_jaccardMtN)

            noisesFullScoreDict[noise][0].append(nlaws)
            noisesFullScoreDict[noise][1].append(fullscore)

            noisesRMSEDict[noise][0].append(nlaws)
            noisesRMSEDict[noise][1].append(rmse / variableRange)

            nRulesDict[noise][0].append(nlaws)
            nRulesDict[noise][1].append(avgRules)

            fullJaccardDict[noise][0].append(nlaws)
            fullJaccardDict[noise][1].append(avgfull_jaccard)

            noisesFullScoreDictMt[noise][0].append(nlaws)
            noisesFullScoreDictMt[noise][1].append(fullscoreMt)

            noisesRMSEDictMt[noise][0].append(nlaws)
            noisesRMSEDictMt[noise][1].append(avgRulesMt)

            nRulesDictMt[noise][0].append(nlaws)
            nRulesDictMt[noise][1].append(avgRulesMt)

            fullJaccardDictMt[noise][0].append(nlaws)
            fullJaccardDictMt[noise][1].append(avgfull_jaccardMt)

            if small_mts :
                noisesFullScoreDictMtN[noise][0].append(nlaws)
                noisesFullScoreDictMtN[noise][1].append(fullscoreMtN)

                noisesRMSEDictMtN[noise][0].append(nlaws)
                noisesRMSEDictMtN[noise][1].append(avgRulesMtN)

                nRulesDictMtN[noise][0].append(nlaws)
                nRulesDictMtN[noise][1].append(avgRulesMtN)

                fullJaccardDictMtN[noise][0].append(nlaws)
                fullJaccardDictMtN[noise][1].append(avgfull_jaccardMtN)


        fig, axes = plt.subplots(figsize=(20, 10))
        axes.plot(noises, fullscores2plot)
        axes.plot(noises, fullscores2plotMt)
        if small_mts:
            axes.plot(noises, fullscores2plotMtN)
        axes.set_title('F1-cosine-jaccard')
        axes.set_xlabel('Noise')
        if small_mts :
            plt.legend(['Hipar', 'MTs', 'MTs(n)'])
        else:
            plt.legend(['Hipar', 'MTs'])
        plt.savefig('artificial_dataset_f1_cosine_jaccard_' + str(nlaws) + '_laws.png')

        fig, axes = plt.subplots(figsize=(20, 10))
        axes.plot(noises, full_jaccardplot)
        axes.plot(noises, full_jaccardplotMt)
        if small_mts :
            axes.plot(noises, full_jaccardplotMtN)
        axes.set_title('Jaccard-cosine')
        axes.set_xlabel('Noise')
        if small_mts:
            plt.legend(['Hipar', 'MTs', 'MTs(n)'])
        else:
            plt.legend(['Hipar', 'MTs'])
        plt.savefig('artificial_dataset_jaccard_cosine_' + str(nlaws) + '_laws.png')


        fig, axes = plt.subplots(figsize=(20, 10))
        bplot2 = axes.plot(noises, nrulesplot)
        axes.plot(noises, nrulesplotMt)
        if small_mts :
            axes.plot(noises, nrulesplotMtN)
        axes.set_title('Number of rules')
        axes.set_xlabel('Noise')
        if small_mts:
            plt.legend(['Hipar', 'MTs', 'MTs(n)'])
        else:
            plt.legend(['Hipar', 'MTs'])

        plt.savefig('artificial_dataset_nrules' + str(nlaws) + '_laws.png')

        fig, axes = plt.subplots(figsize=(20, 10))
        bplot2 = axes.plot(noises, rmseplot)
        axes.plot(noises, rmseplotMt)
        if small_mts :
            axes.plot(noises, rmseplotMtN)
        axes.set_title('RMSD')
        axes.set_xlabel('Noise')
        if small_mts:
            plt.legend(['Hipar', 'MTs', 'MTs(n)'])
        else:
            plt.legend(['Hipar', 'MTs'])
        plt.savefig('artificial_dataset_rmse' + str(nlaws) + '_laws.png')


    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(noisesFullScoreDict[0.2][0], noisesFullScoreDict[0.2][1])
    axes.plot(noisesFullScoreDictMt[0.2][0], noisesFullScoreDictMt[0.2][1])
    if small_mts :
        axes.plot(noisesFullScoreDictMtN[0.2][0], noisesFullScoreDictMtN[0.2][1])
    axes.set_title('Hmean precision-recall-cosine@noise=0.2')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_2_f1-jaccard-cosine_laws.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(noisesFullScoreDict[0.0][0], noisesFullScoreDict[0.0][1])
    axes.plot(noisesFullScoreDictMt[0.0][0], noisesFullScoreDictMt[0.0][1])
    if small_mts :
        axes.plot(noisesFullScoreDictMtN[0.0][0], noisesFullScoreDictMtN[0.0][1])
    axes.set_title('Hmean precision-recall-cosine@noise=0.0')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_0_f1-jaccard-cosine_laws.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(noisesFullScoreDict[0.4][0], noisesFullScoreDict[0.0][1])
    axes.plot(noisesFullScoreDictMt[0.4][0], noisesFullScoreDictMt[0.4][1])
    if small_mts :
        axes.plot(noisesFullScoreDictMtN[0.4][0], noisesFullScoreDictMtN[0.4][1])
    axes.set_title('Hmean precision-recall-cosine@noise=0.0')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_4_f1-jaccard-cosine_laws.png')


    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(noisesRMSEDict[0.2][0], noisesRMSEDict[0.2][1])
    if small_mts :
        axes.plot(noisesRMSEDictMtN[0.2][0], noisesRMSEDictMtN[0.2][1])
    axes.set_title('RMSD@noise=0.2')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_2_rmsd_laws.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(noisesRMSEDict[0.0][0], noisesRMSEDict[0.0][1])
    axes.plot(noisesRMSEDictMt[0.0][0], noisesRMSEDictMt[0.0][1])
    if small_mts :
        axes.plot(noisesRMSEDictMtN[0.0][0], noisesRMSEDictMtN[0.0][1])
    axes.set_title('RMSD@noise=0.0')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_0_rmsd_laws.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(noisesRMSEDict[0.4][0], noisesRMSEDict[0.0][1])
    axes.plot(noisesRMSEDictMt[0.4][0], noisesRMSEDictMt[0.0][1])
    if small_mts :
        axes.plot(noisesRMSEDictMtN[0.4][0], noisesRMSEDictMtN[0.0][1])
    axes.set_title('RMSD@noise=0.0')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_4_rmsd_laws.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(nRulesDict[0.2][0], nRulesDict[0.2][1])
    axes.plot(nRulesDictMt[0.2][0], nRulesDictMt[0.2][1])
    if small_mts :
        axes.plot(nRulesDictMtN[0.2][0], nRulesDictMtN[0.2][1])
    axes.set_title('Rules found@noise=0.2')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_2_rules_laws.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(nRulesDict[0.0][0], nRulesDict[0.0][1])
    axes.plot(nRulesDictMt[0.0][0], nRulesDictMt[0.0][1])
    if small_mts :
        axes.plot(nRulesDictMtN[0.0][0], nRulesDictMtN[0.0][1])
    axes.set_title('Rules found@noise=0.0')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_0_rules_laws.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(nRulesDict[0.4][0], nRulesDict[0.0][1])
    axes.plot(nRulesDictMt[0.4][0], nRulesDictMt[0.0][1])
    if small_mts :
        axes.plot(nRulesDictMtN[0.4][0], nRulesDictMtN[0.0][1])
    axes.set_title('Rules found@noise=0.0')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_4_rules_laws.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(fullJaccardDict[0.2][0], fullJaccardDict[0.2][1])
    axes.plot(fullJaccardDictMt[0.2][0], fullJaccardDictMt[0.2][1])
    if small_mts :
        axes.plot(fullJaccardDictMtN[0.2][0], fullJaccardDictMtN[0.2][1])
    axes.set_title('Jaccard-cosine@noise=0.2')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_2_jaccard_cosine.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(fullJaccardDict[0.0][0], fullJaccardDict[0.0][1])
    axes.plot(fullJaccardDictMt[0.0][0], fullJaccardDictMt[0.0][1])
    if small_mts :
        axes.plot(fullJaccardDictMtN[0.0][0], fullJaccardDictMtN[0.0][1])
    axes.set_title('Jaccard-cosine@noise=0.0')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_0_jaccard_cosine.png')

    fig, axes = plt.subplots(figsize=(20, 10))
    bplot2 = axes.plot(fullJaccardDict[0.4][0], fullJaccardDict[0.0][1])
    axes.plot(fullJaccardDictMt[0.4][0], fullJaccardDictMt[0.0][1])
    if small_mts :
        axes.plot(fullJaccardDictMtN[0.4][0], fullJaccardDictMtN[0.0][1])
    axes.set_title('Jaccard-cosine@noise=0.0')
    axes.set_xlabel('Number of rules')
    if small_mts:
        plt.legend(['Hipar', 'MTs', 'MTs(n)'])
    else:
        plt.legend(['Hipar', 'MTs'])
    plt.savefig('artificial_dataset_noise_0_4_jaccard_cosine.png')

def experimentsSimple(small_mts=False, ntimes=3) :
    error_metric = 'rmse'
    noises = [round(x, 1) for x in numpy.arange(0.0, 1.1, 0.1)]
    noisesFullScoreDict = {}
    noisesRMSEDict = {}
    nRulesDict = {}
    fullJaccardDict = {}

    noisesFullScoreDictMt = {}
    noisesRMSEDictMt = {}
    nRulesDictMt = {}
    fullJaccardDictMt = {}

    noisesFullScoreDictMtN = {}
    noisesRMSEDictMtN = {}
    nRulesDictMtN = {}
    fullJaccardDictMtN = {}
    nlaws = 2

    fullscores2plot = []
    rmseplot = []
    nrulesplot = []
    full_jaccardplot = []

    fullscores2plotMt = []
    rmseplotMt = []
    nrulesplotMt = []
    full_jaccardplotMt = []

    fullscores2plotMtN = []
    rmseplotMtN = []
    nrulesplotMtN = []
    full_jaccardplotMtN = []

    the_seed = datetime.now()

    for noise in noises:
        noisesFullScoreDict[noise] = ([], [])
        noisesRMSEDict[noise] = ([], [])
        nRulesDict[noise] = ([], [])
        fullJaccardDict[noise] = ([], [])

        noisesFullScoreDictMt[noise] = ([], [])
        noisesRMSEDictMt[noise] = ([], [])
        nRulesDictMt[noise] = ([], [])
        fullJaccardDictMt[noise] = ([], [])

        if small_mts :
            noisesFullScoreDictMtN[noise] = ([], [])
            noisesRMSEDictMtN[noise] = ([], [])
            nRulesDictMtN[noise] = ([], [])
            fullJaccardDictMtN[noise] = ([], [])

        #def gen_dataset(law_min_size, law_max_size, nsymb, nnum, nlaws, noise, seed=42, print_infos=True):
        df, laws, lawsizes = gen_dataset(100, 100, 1, 1, nlaws, noise, seed=the_seed)
        #print(laws)
        maximum = numpy.max(df['target'])
        minimum = numpy.min(df['target'])
        variableRange = maximum - minimum
        min_support = min(lawsizes) / (2 * len(df))

        rmses, maes, precisions, recalls, f1s, r2s, cosines, fullscores, n_rules, jaccards, \
        full_jaccards = evaluate_artificial_dataset(df, laws, min_support=min_support,
                                                    verbose=False, error_metric=error_metric, fix_symb_attrs=True,
                                                    ntimes=ntimes, small_mts=small_mts, exp_type='simple')

        fullscore = numpy.mean(fullscores['hipar'])
        avgRules = numpy.mean(n_rules['hipar'])
        rmse = numpy.mean(rmses['hipar'])
        rmsd_std = numpy.std(numpy.array(rmses['hipar']) / variableRange)
        avgfull_jaccard = numpy.mean(full_jaccards['hipar'])

        fullscoreMt = numpy.mean(fullscores['mt'])
        avgRulesMt = numpy.mean(n_rules['mt'])
        rmseMt = numpy.median(rmses['mt'])
        avgfull_jaccardMt = numpy.mean(full_jaccards['mt'])

        if small_mts :
            fullscoreMtN = numpy.mean(fullscores['mtn'])
            avgRulesMtN = numpy.mean(n_rules['mtn'])
            rmseMtN = numpy.median(rmses['mtn'])
            avgfull_jaccardMtN = numpy.mean(full_jaccards['mtn'])


        print('HIPAR: nlaws=', nlaws, 'noise=', noise, 'rmse=', rmse, 'mae=', numpy.mean(maes['hipar']),
              'precision=', numpy.mean(precisions['hipar']), 'recall=', numpy.mean(recalls['hipar']), 'f1=', numpy.mean(f1s['hipar']),
              'r2=', numpy.mean(r2s['hipar']), 'cosine=', numpy.mean(cosines['hipar']), 'fs=', fullscore,
                                                        'nr=', avgRules, 'jacc=', numpy.mean(jaccards['hipar']),
                                                        'fulljacc=', avgfull_jaccard)
        print('MTs nlaws=', nlaws, 'noise=', noise, 'rmse=', rmseMt, 'mae=', numpy.mean(maes['mt']),
              'precision=', numpy.mean(precisions['mt']), 'recall=', numpy.mean(recalls['mt']), 'f1=',
              numpy.mean(f1s['mt']),
              'r2=', numpy.mean(r2s['mt']), 'cosine=', numpy.mean(cosines['mt']), 'fs=', fullscoreMt,
              'nr=', avgRulesMt, 'jacc=', numpy.mean(jaccards['mt']),
              'fulljacc=', avgfull_jaccardMt)

        if small_mts :
            print('MTs(n) nlaws=', nlaws, 'noise=', noise, 'rmse=', rmseMtN, 'mae=', numpy.mean(maes['mtn']),
                  'precision=', numpy.mean(precisions['mtn']), 'recall=', numpy.mean(recalls['mtn']), 'f1=',
                  numpy.mean(f1s['mtn']),
                  'r2=', numpy.mean(r2s['mtn']), 'cosine=', numpy.mean(cosines['mtn']), 'fs=', fullscoreMtN,
                  'nr=', avgRulesMtN, 'jacc=', numpy.mean(jaccards['mtn']),
                  'fulljacc=', avgfull_jaccardMtN)

        fullscores2plot.append(fullscore)
        rmseplot.append(rmse / variableRange)
        nrulesplot.append(avgRules)
        full_jaccardplot.append(avgfull_jaccard)

        fullscores2plotMt.append(fullscoreMt)
        rmseplotMt.append(rmseMt / variableRange)
        nrulesplotMt.append(avgRulesMt)
        full_jaccardplotMt.append(avgfull_jaccardMt)

        if small_mts :
            fullscores2plotMtN.append(fullscoreMtN)
            rmseplotMtN.append(rmseMtN / variableRange)
            nrulesplotMtN.append(avgRulesMtN)
            full_jaccardplotMtN.append(avgfull_jaccardMtN)

        noisesFullScoreDict[noise][0].append(nlaws)
        noisesFullScoreDict[noise][1].append(fullscore)

        noisesRMSEDict[noise][0].append(nlaws)
        noisesRMSEDict[noise][1].append(rmse / variableRange)

        nRulesDict[noise][0].append(nlaws)
        nRulesDict[noise][1].append(avgRules)

        fullJaccardDict[noise][0].append(nlaws)
        fullJaccardDict[noise][1].append(avgfull_jaccard)

        noisesFullScoreDictMt[noise][0].append(nlaws)
        noisesFullScoreDictMt[noise][1].append(fullscoreMt)

        noisesRMSEDictMt[noise][0].append(nlaws)
        noisesRMSEDictMt[noise][1].append(avgRulesMt)

        nRulesDictMt[noise][0].append(nlaws)
        nRulesDictMt[noise][1].append(avgRulesMt)

        fullJaccardDictMt[noise][0].append(nlaws)
        fullJaccardDictMt[noise][1].append(avgfull_jaccardMtN)

        if small_mts :
            noisesFullScoreDictMtN[noise][0].append(nlaws)
            noisesFullScoreDictMtN[noise][1].append(fullscoreMtN)

            noisesRMSEDictMtN[noise][0].append(nlaws)
            noisesRMSEDictMtN[noise][1].append(avgRulesMtN)

            nRulesDictMtN[noise][0].append(nlaws)
            nRulesDictMtN[noise][1].append(avgRulesMtN)

            fullJaccardDictMtN[noise][0].append(nlaws)
            fullJaccardDictMtN[noise][1].append(avgfull_jaccardMtN)

    fig, axes = plt.subplots(figsize=(10, 7))
    axis_font = {'size': '18'}
    axes.plot(noises, fullscores2plot)
    axes.plot(noises, fullscores2plotMt)
    if small_mts :
        axes.plot(noises, fullscores2plotMtN)
    axes.set_title('F1-cosine-jaccard')
    axes.set_xlabel('Noise', axis_font)
    if small_mts :
        plt.legend(['Hipar', 'MT', 'MT+H'])
    else:
        plt.legend(['Hipar', 'MT'])

    for label in (axes.get_xticklabels() + axes.get_yticklabels()):
        label.set_fontsize(18)

    plt.savefig('simple_artificial_dataset_f1_cosine_jaccard_' + str(nlaws) + '_laws.png')

    fig, axes = plt.subplots(figsize=(10, 7))
    axes.plot(noises, full_jaccardplot)
    axes.plot(noises, full_jaccardplotMt)
    if small_mts :
        axes.plot(noises, full_jaccardplotMtN)
    axes.set_title('Jaccard-cosine')
    axes.set_xlabel('Noise', axis_font)
    if small_mts:
        plt.legend(['Hipar', 'MT', 'MT+H'])
    else:
        plt.legend(['Hipar', 'MT'])

    for label in (axes.get_xticklabels() + axes.get_yticklabels()):
        label.set_fontsize(18)

    plt.savefig('simple_artificial_dataset_jaccard_cosine_' + str(nlaws) + '_laws.png')

    fig, axes = plt.subplots(figsize=(10, 7))
    bplot2 = axes.plot(noises, nrulesplot)
    axes.plot(noises, nrulesplotMt)
    if small_mts :
        axes.plot(noises, nrulesplotMtN)
    axes.set_title('Number of rules')
    axes.set_xlabel('Noise', axis_font)
    if small_mts:
        plt.legend(['Hipar', 'MT', 'MT+H'])
    else:
        plt.legend(['Hipar', 'MT'])
    for label in (axes.get_xticklabels() + axes.get_yticklabels()):
        label.set_fontsize(18)
    plt.savefig('simple_artificial_dataset_nrules' + str(nlaws) + '_laws.png')

    fig, axes = plt.subplots(figsize=(10, 7))
    bplot2 = axes.plot(noises, rmseplot)
    #plt.fill_between(noises, rmseplot - rmsd_std, rmseplot + rmsd_std, color='grey', alpha='0.5')
    axes.plot(noises, rmseplotMt)
    if small_mts :
        axes.plot(noises, rmseplotMtN)

    axes.set_title('RMSD')
    axes.set_xlabel('Noise', axis_font)
    if small_mts:
        plt.legend(['Hipar', 'MT', 'MT+H'])
    else:
        plt.legend(['Hipar', 'MT', 'MT+H'])

    for label in (axes.get_xticklabels() + axes.get_yticklabels()):
        label.set_fontsize(18)

    plt.savefig('simple_artificial_dataset_rmse' + str(nlaws) + '_laws.png')

def experimentsFullWithDiscretization(small_mts=False, ntimes=3) :
    error_metric = 'rmse'
    noises = [round(x, 1) for x in numpy.arange(0.0, 1.1, 0.1)]
    noisesRMSEDict = {}
    nRulesDict = {}

    noisesRMSEDictMt = {}
    nRulesDictMt = {}

    noisesRMSEDictMtN = {}
    nRulesDictMtN = {}

    the_seed = datetime.now()

    for noise in noises :
        noisesRMSEDict[noise] = ([], [])
        nRulesDict[noise] = ([], [])

        noisesRMSEDictMt[noise] = ([], [])
        nRulesDictMt[noise] = ([], [])

        if small_mts :
            noisesRMSEDictMtN[noise] = ([], [])
            nRulesDictMtN[noise] = ([], [])

    for nlaws in range(2, 11) :
        rmseplot = []
        nrulesplot = []

        rmseplotMt = []
        nrulesplotMt = []

        if small_mts:
            rmseplotMtN = []
            nrulesplotMtN = []

        for noise in noises :
            df, laws, lawsizes = gen_dataset(30, 30, 2, 3, nlaws, noise, seed=the_seed)
            maximum = numpy.max(df['target'])
            minimum = numpy.min(df['target'])
            variableRange = maximum - minimum
            min_support = min(lawsizes) / (len(df) * 2)
            rmses, maes, precisions, recalls, f1s, r2s, cosines, fullscores, n_rules, jaccards, \
            full_jaccards = evaluate_artificial_dataset(df, laws, min_support=min_support,
                                                        verbose=False, error_metric=error_metric,
                                                        small_mts=small_mts, ntimes=ntimes, exp_type='full-disc')
            avgRules = numpy.mean(n_rules['hipar'])
            rmse = numpy.median(rmses['hipar'])

            avgRulesMt = numpy.mean(n_rules['mt'])
            rmseMt = numpy.median(rmses['mt'])

            if small_mts :
                fullscoreMtN = numpy.mean(fullscores['mtn'])
                avgRulesMtN = numpy.mean(n_rules['mtn'])
                rmseMtN = numpy.median(rmses['mtn'])
                avgfull_jaccardMtN = numpy.mean(full_jaccards['mtn'])

            print('HIPAR: nlaws=', nlaws, 'noise=', noise, 'rmse=', rmse, 'mae=', numpy.mean(maes['hipar']),
                  'precision=', numpy.mean(precisions['hipar']), 'recall=', numpy.mean(recalls['hipar']), 'f1=',
                  numpy.mean(f1s['hipar']),
                  'r2=', numpy.mean(r2s['hipar']), 'cosine=', numpy.mean(cosines['hipar']),
                  'nr=', avgRules, 'jacc=', numpy.mean(jaccards['hipar']))

            print('MTs nlaws=', nlaws, 'noise=', noise, 'rmse=', rmseMt, 'mae=', numpy.mean(maes['mt']),
                  'precision=', numpy.mean(precisions['mt']), 'recall=', numpy.mean(recalls['mt']), 'f1=',
                  numpy.mean(f1s['mt']),
                  'r2=', numpy.mean(r2s['mt']), 'cosine=', numpy.mean(cosines['mt']),
                  'nr=', avgRulesMt, 'jacc=', numpy.mean(jaccards['mt']))

            if small_mts :
                print('MTs(n) nlaws=', nlaws, 'noise=', noise, 'rmse=', rmseMtN, 'mae=', numpy.mean(maes['mtn']),
                      'precision=', numpy.mean(precisions['mtn']), 'recall=', numpy.mean(recalls['mtn']), 'f1=',
                      numpy.mean(f1s['mtn']),
                      'r2=', numpy.mean(r2s['mtn']), 'cosine=', numpy.mean(cosines['mtn']), 'fs=', fullscoreMtN,
                      'nr=', avgRulesMtN, 'jacc=', numpy.mean(jaccards['mtn']),
                      'fulljacc=', avgfull_jaccardMtN)

            rmseplot.append(rmse / variableRange)
            nrulesplot.append(avgRules)

            rmseplotMt.append(rmseMt / variableRange)
            nrulesplotMt.append(avgRulesMt)

            if small_mts :
                rmseplotMtN.append(rmseMtN / variableRange)
                nrulesplotMtN.append(avgRulesMtN)

            noisesRMSEDict[noise][0].append(nlaws)
            noisesRMSEDict[noise][1].append(rmse / variableRange)

            nRulesDict[noise][0].append(nlaws)
            nRulesDict[noise][1].append(avgRules)

            noisesRMSEDictMt[noise][0].append(nlaws)
            noisesRMSEDictMt[noise][1].append(avgRulesMt)

            nRulesDictMt[noise][0].append(nlaws)
            nRulesDictMt[noise][1].append(avgRulesMt)

            if small_mts :
                noisesRMSEDictMtN[noise][0].append(nlaws)
                noisesRMSEDictMtN[noise][1].append(avgRulesMtN)

                nRulesDictMtN[noise][0].append(nlaws)
                nRulesDictMtN[noise][1].append(avgRulesMtN)

        fig, axes = plt.subplots(figsize=(20, 10))
        bplot2 = axes.plot(noises, nrulesplot)
        axes.plot(noises, nrulesplotMt)
        if small_mts :
            axes.plot(noises, nrulesplotMtN)
        axes.set_title('Number of rules')
        axes.set_xlabel('Noise')
        if small_mts:
            plt.legend(['Hipar', 'MTs', 'MTs(n)'])
        else:
            plt.legend(['Hipar', 'MTs'])

        plt.savefig('artificial_dataset_nrules' + str(nlaws) + '_laws.png')

        fig, axes = plt.subplots(figsize=(20, 10))
        bplot2 = axes.plot(noises, rmseplot)
        axes.plot(noises, rmseplotMt)
        if small_mts :
            axes.plot(noises, rmseplotMtN)
        axes.set_title('RMSD')
        axes.set_xlabel('Noise')
        if small_mts:
            plt.legend(['Hipar', 'MTs', 'MTs(n)'])
        else:
            plt.legend(['Hipar', 'MTs'])
        plt.savefig('artificial_dataset_rmse' + str(nlaws) + '_laws.png')




if __name__ == '__main__' :
    #experimentsFullWithDiscretization(small_mts=True, ntimes=1)
    #experimentsFull(small_mts=True, ntimes=3)
    experimentsSimple(small_mts=True, ntimes=1)
