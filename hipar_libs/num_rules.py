#!/usr/bin/env python3

import pandas
import numpy
import sys
import pickle
import io
import functools
import time
import random
import math
import itertools
import time

import warnings

#Customs modules
sys.path.append('./hipar_libs/')
sys.path.append('./emm/')

import file_utils
import sparse_linear
import naive_rules
import diversity_utils
import exp_utils

from mdlp.discretization import MDLP
from sklearn import metrics

def queryDataset(df, queryStr) :
    parts = queryStr.split(' and ')
    if len(parts) >= 32 :
        return df.query(queryStr, inplace=False, engine='python')
    else :
        return df.query(queryStr)

## Greedy approach that just takes the best q models in terms
## of ratio support/error (both metrics are normalized)
def select_best_models_greedy(models, data, q, support_bias=1.0) :
    num_rules = len(models)
    errors = numpy.array([m[5] for m in models])
    supports = numpy.array([m[3] for m in models])
    errors = errors / sum(errors)
    errors = numpy.clip(errors, 0.005, 1)
    supports = supports / sum(supports)
    scores = numpy.power(supports, support_bias) / errors

    list2sort = []
    for i in range(len(models)) :
        list2sort.append((i, scores[i]))

    sortedList = sorted(list2sort, key=lambda x : x[1], reverse=True)
    #print(sortedList)

    chosen_models = []
    for i in range(min(q, len(sortedList))) :
        chosen_models.append(sortedList[i][0])

    return chosen_models

def get_1_frequent_id(data, minsup, data_size):
    freq = {}
    freq_vals = {}
    attributes = list(data)
    
    non_freq_attr = []
    
    for a in attributes:
        if not (a in freq.keys()):
            freq[a] = {}
            freq_vals[a] = []
            
        count = dict(data[a].value_counts())
        for v in count:
            if (count[v]/data_size) >= minsup:
                freq[a][v] = list(data.loc[data[a] == v].index)
                freq_vals[a].append(v)
        
        if len(freq_vals[a]) == 0:
            non_freq_attr.append(a)
            freq.pop(a)
            freq_vals.pop(a)

    return freq, freq_vals, non_freq_attr


# Only for symbolic attributes (that's why it need 'symb_attr' to know which are symbolics)
def get_local_frequents_val(attributes, symb_attr, freq_attrs_val, sub_data):
    local_freq_attr_val = {}
    
    attrs = [a for a in attributes if a in symb_attr]
    for a in attrs:
        if not (a in local_freq_attr_val.keys()):
            local_freq_attr_val[a] = []
        
        enum = pandas.unique(sub_data[a].values)
        local_freq_attr_val[a] = list(set(enum).intersection(freq_attrs_val[a]))
    
    return local_freq_attr_val

    
def one_closed(freq_is):
    ca = []
    attrs = list(freq_is.keys())
    for a in attrs:
        for v in freq_is[a]:
            common = {a:v}
            sa = freq_is[a][v]
            for a2 in attrs[attrs.index(a)+1:]:            
                for v2 in freq_is[a2]:
                    sb = freq_is[a2][v2]                    
                    tmp_inds = list(numpy.intersect1d(sa, sb, assume_unique=True))
                    incl = (tmp_inds == sa) if len(sa) == len(tmp_inds) else False
                    if incl:
                        common[a2] = v2
            ca.append((common, list(common)[0], sorted(list(sa))))
    return ca


def cld_get_1_frequent_id(data, minsup, data_size):
    '''
    Get the frequent items (couple (var:value)) of size 1 needed to start the computations
    
    Keyword arguments:
    data -- the dataset in a dataframe format
    minsup -- the minimum support
    data_size -- the size of the dataset
    '''
    freq = {}
    attributes = list(data)
    
    non_freq_attr = []
    
    for a in attributes:
        if not (a in freq.keys()):
            freq[a] = {}
            
        count = dict(data[a].value_counts())
        for v in count:
            if (count[v]/data_size) >= minsup:
                freq[a][v] = list(data.loc[data[a] == v].index)
        
        if len(freq[a]) == 0:
            non_freq_attr.append(a)
            freq.pop(a)
    
    return freq, non_freq_attr
    

def rightx(ix, attrs):
    if ix is None:
        return [attrs[0]]
    else:
        return attrs[(attrs.index(ix) + 1):]


def cld_discretized_features(attributes, sub_data, minsup, data_size):
    '''
    Discretize numeric attributes b splitting w.r.t. the median
    
    Keyword arguments:
    attributes -- the attributes to discretize
    sub_data -- the data to discretize (pandas dataframe)
    minsup -- the minimum support (not really usefull ?)
    data_size -- the size of the whole dataset
    '''
    vals_inds = {}
    num_feat = {}
    for a in attributes:
        num_feat[a] = []
        m = sub_data[a].median()
        v1 = a + '<' + str(round(m,4))
        v2 = a + '>=' + str(round(m,4))
        
        ic1 = sub_data.loc[sub_data[a] < m].index
        ic2 = sub_data.loc[sub_data[a] >= m].index
                
        if (len(ic1)/data_size) >= minsup:
            num_feat[a].append((a,v1))
            vals_inds[(a,v1)] = list(ic1)
        
        if (len(ic2)/data_size) >= minsup:
            num_feat[a].append((a,v2))
            vals_inds[(a,v2)] = list(ic2)
            
        if len(num_feat[a]) == 0:
            num_feat.pop(a)
    
    return vals_inds, num_feat


def interval_tostring(attribute, interval, minv, maxv):
    '''
    Small function to represent an interval as a string (latter usable to construct a pandas query)
    
    Keyword arguments:
    attribute -- the attribute name concerned by the interval
    interval -- a tuple representing the interval
    minv -- the maximum value possible for this attribute
    maxv -- the minimum value possible for this attribute
    '''
    s = ''
    if (interval[0] == -numpy.inf) and (interval[1] == numpy.inf):
        #s += attribute + '>=' + str(minv) + ' and ' + attribute + '<' + str(maxv+1)
        s += 'None'
    elif interval[0] == -numpy.inf:
        s += attribute + '<' + str(interval[1])
    elif interval[1] == numpy.inf:
        s += attribute + '>=' + str(interval[0])
    else:
        s += attribute + '>=' + str(interval[0]) + ' and ' + attribute + '<' + str(interval[1])
    return s


def discretize_mdlp(attributes, sub_data, minsup, data_size, inds_SE, inds_LE, force_discretize=False):
    vals_inds = {}
    num_feat = {}    
    
    data = sub_data[attributes].copy()
    data['target'] = data.index
    data['target'] = data['target'].map(lambda x: 0 if x in inds_SE else 1)
    X = numpy.array(data[attributes])
    y = numpy.array(data['target'])

    if force_discretize :
        mdlp = MDLP(min_depth=1)#random_state=42)
    else :
        mdlp = MDLP()
    #mdlp = MDLP()
    conv_X = numpy.array(mdlp.fit_transform(X, y))
    
    for i in range(conv_X.shape[1]):
        a = attributes[i]
        num_feat[a] = []
        minv = data[a].min()
        maxv = data[a].max()
        intervals = mdlp.cat2intervals(conv_X, i)
        for j,v in enumerate(intervals):
            vs = interval_tostring(a, v, minv, maxv)
            if vs != 'None':
                if (a,vs) not in num_feat[a]:
                    num_feat[a].append((a,vs))
                if (a,vs) not in vals_inds:
                    vals_inds[(a,vs)] = []
                vals_inds[(a,vs)].append(data.index[j])
    
    return vals_inds, num_feat

def prune_discretizations(vals_inds, num_feat, data, target, minsup) :
    miu = numpy.mean(data[target])
    data_size = len(data)
    intervals = []
    property_idx = {}
    for a in num_feat:
        property_idx[a] = []
        for v in num_feat[a]:
            pattern = ([v], a, vals_inds[v])
            ids_in_class = vals_inds[v]
            ids_in_complement = data.index.difference(ids_in_class)
            if len(ids_in_class) == 0 or len(ids_in_complement) == 0 :
                continue

            support_in_class = len(ids_in_class) / data_size
            if support_in_class < minsup :
                continue

            miu, support, icv = exp_utils.interclass_variance(data[target].loc[ids_in_class],
                                                              data[target].loc[ids_in_complement], miu)
            intervals.append((pattern, icv, miu, support))
            property_idx[a].append((pattern, icv, miu, support))

    if len(intervals) == 0 :
        return vals_inds, num_feat, None, None

    intervals = sorted(intervals, key=lambda x : -x[1])
    maxicv = intervals[0][1]
    ## Take intervals in the first quartile
    cutPoint = min(len(intervals) - 1, int(math.ceil(len(intervals) * 0.15)))
    #cutPoint = 1
    chosen_intervals = list(intervals[:cutPoint])
    minicv = intervals[cutPoint - 1][1]

    # Now look at their complements
    for chosen_interval in intervals[:cutPoint] :
        #print('The chosen interval', chosen_interval[0][0][0][1], chosen_interval[1], chosen_interval[2], chosen_interval[3])
        predicate = chosen_interval[0][1]
        for interval in property_idx[predicate] :
            ## If it is not the same interval add it!
            #print('Testing for', interval[0][0][0][1], interval[1], interval[2], interval[3])
            if interval[2] != chosen_interval[2] or \
                    interval[3] != chosen_interval[3] :
                support_in_class = chosen_interval[3] / data_size
                #print('Considering', interval[0][0][0][1], interval[1], interval[2], interval[3])
                if support_in_class >= minsup:
                    #print('Adding', interval[0][0][0][1], interval[1], interval[2], interval[3])
                    chosen_intervals.append(interval)


    new_vals_inds = {}
    new_num_feat = {}

    lastMiu = None
    lastSupport = None
    lasticv = None
    for interval in chosen_intervals :
        miu = interval[2]
        support = interval[3]
        icv = interval[1]

        if lastMiu is not None and math.isclose(icv, lasticv) and math.isclose(miu, lastMiu) \
                and support == lastSupport :
            continue

        lasticv = icv
        lastSupport = support
        lastMiu = miu
        #if lasticv < minicv :
        #    minicv = lasticv

        a = interval[0][1]
        vs = interval[0][0][0][1]
        tp = (a, vs)
        if a not in new_num_feat :
            new_num_feat[a] = []
        if tp not in new_vals_inds :
            new_vals_inds[tp] = []

        new_num_feat[a].append(tp)
        new_vals_inds[tp] = interval[0][2]

    return new_vals_inds, new_num_feat, maxicv, minicv


def discretize_features(attributes, sub_data, minsup, data_size, target, inds_SE=None, inds_LE=None,
                        discretize_method='median', force_discretize=False):
    if len(attributes) > 0:
        if discretize_method == 'mdlp' or discretize_method == 'mdlp-tv':
            vals_inds, num_feat =  discretize_mdlp(attributes, sub_data, minsup, data_size, inds_SE, inds_LE, force_discretize)
            vals_inds, num_feat, maxicv, minicv = prune_discretizations(vals_inds, num_feat, sub_data, target, minsup)
            return vals_inds, num_feat, maxicv, minicv
        elif discretize_method == 'median':
            vals_inds, num_feat = cld_discretized_features(attributes, sub_data, minsup, data_size)
            return vals_inds, num_feat, None, None
        else:
            raise ValueError('Unsupported discretization method : ' + discretize_method)
    else:
        return {}, {}, None, None
    

def itemized_data(data, attributes):
    """Transform the given dataframe by replacing each value by a tuple (attribute, value)"""
    tmpdata = data.copy()
    for c in attributes:
        tmpc = []
        for e in tmpdata[c]:
            tmpe = (c,e)
            tmpc.append(tmpe)
        tmpdata[c] = tmpc
    return tmpdata


def closure(inds, num_inds, data, all_attrs, parent):
    """Find the closed pattern corresponding to the subdata corresponding to the indexes inds
    
    Keyword arguments:
    inds -- list of indexes describing a subset of the data
    num_inds -- list of indexes corresponding to discretized values (if discretization is used)
    data -- the dataset in a pandas dataframe format
    all_attrs -- the attributes of the dataset
    """
    #a = numpy.array(data.iloc[inds])
    a = numpy.array(data)
    i = functools.reduce(numpy.intersect1d, a)
    rs = list(i)
    if num_inds is not None:
        rn_inds = [e for e in num_inds if num_inds[e] == inds]
        rs = rs + rn_inds
    
    if all_attrs is not None and parent is not None:
        closed = parent + [x for x in rs if x not in parent]
        closed = sorted(closed, key=lambda x:all_attrs.index(x[0]))
    else:
        closed = rs
    return closed


def is_first_parent(closure, parent):
    return closure[:len(parent)] == parent


def split_SE_LE(inds, res):
    tmp_inds = numpy.array(inds)
    #ref_error = numpy.median(res)
    ref_error = numpy.mean(res)
    inds_SE = tmp_inds[numpy.where(res < ref_error)]
    inds_LE = tmp_inds[numpy.where(res >= ref_error)]
    return inds_SE, inds_LE

def split_SV_LV(data, target) :
    ref_val = numpy.median(data[target])
    inds_SE = data.index[numpy.where(data[target] < ref_val)]
    inds_LE = data.index[numpy.where(data[target] >= ref_val)]
    return inds_SE, inds_LE

def pattern_query(pattern, num_attrs):
    qr = ''
    for k in pattern:
        #qr += str(k)
        if k in num_attrs: # If true, then it is an discretized attribute
            qr += pattern[k]
        else: # Symbolic attribute
            qr += str(k) + '==' + '"' + str(pattern[k]) + '"'

        qr += ' and '
    qr = qr[:-5]
    return qr


# (([('CHMAX', 'CHMAX>=16.0')], 'CHMAX'), None, inf, 0.31216931216931215, nan, nan)
# {'Manufacturer': 'SATURN', 'drv': 'F', 'fl': 'R'}
# Check if there is a better parent in term of error
def exist_better_parents(current_model, data, work_data, target, tol, error_metric, reg_type):
    if len(current_model[0]) > 1:
        has_better_parent = False
        child_error = current_model[5]
        num_attrs = list(data.select_dtypes(include=['int64', 'float64']))
        num_attrs.remove(target)
        patternlist = list(current_model[0].items())
        parents = list(itertools.combinations(patternlist, len(patternlist)-1))
        for p in parents:
            qr = pattern_query(dict(p), num_attrs)
            inds = list(queryDataset(data, qr).index)
            if len(inds) > 5 :
                regp, l2errorp, mean_predp, parrent_error, resp, cv_scorep = \
                    sparse_linear.omp_regression(data.iloc[inds], num_attrs, target, tol, error_metric=error_metric,
                                                 reg_type=reg_type, cv=None)
            else :
                parrent_error = None
            if parrent_error is not None:
                pb = (parrent_error <= child_error)
                has_better_parent = has_better_parent or (parrent_error <= child_error)

        return has_better_parent
    
    return False


# Find closed frequent patterns with dynamic discretization and associate a regression to it
# DFS algorithm
def closed_rule_search2(data, target, minsup, num_attrs, symb_attr, discretize=True,
                       discretize_method='mdlp', error_metric='rmse',
                       reduce_output=True, closure_num=True, ignored_attrs_for_disc=None,
                       ignored_sym_conds=None, force_discretize=False):
    """
    Explore the data in order to find hybrid rules
    A rule is made of the following components :
        * An head that is a closed pattern
        * A tail corresponding to a regression model

    Keyword arguments:
    data -- a pandas.DataFrame containing the training data
    target -- the name of the target attribute (the one we want to predict)
    minsup -- minimal support of a rule (minimal percentage of the data a rule need to cover) (value between 0 and 1)
    num_attr -- list of the numericals attributes
    symb_attr -- list of the symbolic attributes
    discretize -- flag to use dynamic dicretization while finding rule, so numerical attributes can be used in the head of the rule. can significantly increase the runtime, depending on the number of numerical attributes (default True)
    discretize_method -- method to use for discretization : either 'mdlp' for Minimum Description Length Binning or 'median' to split w.r.t the median value of the feature (default 'mdlp')
    """

    if discretize_method not in ['mdlp', 'mdlp-tv', 'median'] :
        raise ValueError('Unknown value for parameter discretize_method : \'' +
                         discretize_method + '\', discretize_method take either \'mdlp\' or \'median\' as possible values')


    if (minsup > 1):
        print('Error : minsup must be between 0 and 1')
        return -1

    num_attr = num_attrs.copy()
    targetMean = numpy.mean(data[target])
    if target in num_attr:
        num_attr.remove(target)

    models = []
    data_size = data.shape[0]
    work_data = data.copy()

    freq_ids = None
    freq_vals = []
    closed_1 = []
    minicv = None

    if len(symb_attr) == 0 :
        discretize = True
        force_discretize = True

    if force_discretize :
        print('Discretization at first level enforced')

    num_attrs_for_disc = None
    if ignored_attrs_for_disc is not None:
        num_attrs_for_disc = list(filter(lambda x: x not in ignored_attrs_for_disc, num_attrs))
    else:
        num_attrs_for_disc = num_attrs

    # We discretize the numeric attributes
    if discretize:
        if discretize_method == 'mdlp':
            try:
                reg, l2error, mean_pred, mae, res, cv_score = sparse_linear.omp_regression(work_data, num_attr, target,
                                                                                           0, error_metric=error_metric,
                                                                                           reg_type='lin', cv=None)

                inds_SE, inds_LE = split_SE_LE(work_data.index, res)
            except ValueError:
                inds_SE, inds_LE = [], []
        elif discretize_method == 'mdlp-tv' :
            inds_SE, inds_LE = split_SV_LV(work_data, target)
        else:
            inds_SE, inds_LE = [], []

        vals_inds, num_feat, maxicv, minicv = discretize_features(num_attrs_for_disc, work_data,
                                                                  minsup, data_size, target,
                                                  inds_SE, inds_LE, discretize_method, force_discretize)

        for a in num_feat:
            for v in num_feat[a]:
                closed_1.append(([v], a, vals_inds[v]))


    if len(symb_attr) > 0:
        work_data = itemized_data(work_data, symb_attr)

        freq_ids, freq_vals, non_freq_attr = get_1_frequent_id(work_data[symb_attr], minsup, data_size)

        # Filter the ignored conditions
        if ignored_sym_conds is not None:
            for cond in ignored_sym_conds:
                if cond[0] in freq_ids and cond[0] in freq_vals:
                    freq_ids[cond[0]].pop(cond, None)
                    try:
                        freq_vals[cond[0]].remove(cond)
                    except:
                        pass
                    if len(freq_vals[cond[0]]) == 0:
                        del freq_vals[cond[0]]
                        symb_attr.remove(cond[0])
                    if len(freq_ids[cond[0]]) == 0:
                        del freq_ids[cond[0]]

        for a in freq_ids:
            for i in freq_ids[a]:
                # Avoid patterns that cover the entire dataset
                if (len(freq_ids[a][i]) ==  data_size) :
                    continue

                closedPattern = closure(freq_ids[a][i], None,
                                        work_data.iloc[freq_ids[a][i]][symb_attr],
                                         None, None)


                miu, sup, icv = exp_utils.interclass_variance(work_data[target].iloc[freq_ids[a][i]],
                                              work_data[target].index.difference(work_data[target].iloc[freq_ids[a][i]]),
                                                              targetMean)

                if minicv is not None :
                    if icv > minicv :
                        closed_1.append((closedPattern, a, freq_ids[a][i]))
                else :
                    closed_1.append((closedPattern, a, freq_ids[a][i]))
                #print('Adding', closedPattern)
                #closed_1.append((closedPattern, a, freq_ids[a][i]))

        if len(non_freq_attr) > 0:
            work_data = work_data.drop([a for a in non_freq_attr if a not in num_attr], axis=1)
            symb_attr = [a for a in symb_attr if a not in non_freq_attr]

    all_attr = symb_attr + num_attr
    default_model = None

    stack = []
    stack.append(
        ((([], None), None, numpy.inf, 1, numpy.nan, numpy.nan), freq_vals, list(work_data.index), (0, data_size)))

    for p in closed_1:
        stack.append((((p[0], p[1]), None, numpy.inf, len(p[2]) / data_size, numpy.nan, numpy.nan), freq_vals, p[2],
                      (0, len(p[2]))))

    closure_time = 0
    stop = False

    while not stop:
        current = stack.pop()
        current_model = current[0]
        #print('Current model', current_model)
        current_freq_vals = current[1]
        current_inds = current[2]
        # current_data = current[2]
        # current_inds = list(current_data.index)
        supp = len(current_inds) / data_size
        # supp = current_data.shape[0]

        # Regression models
        # tol = error_stack.pop()
        tol = (current[3][0] / current[3][1]) * len(current_inds)

        if current_model[0][0] == []:
            stop = True

        reg, l2error, mean_pred, mae, res, cv_score, chosen_reg_type = sparse_linear.sparse_regression(
            work_data.iloc[current_inds], num_attr, target, tol, error_metric=error_metric, cv=2)

        msep = (current[3][0] / current[3][1])
        msec = (l2error / len(current_inds))

        error = (l2error, len(current_inds))

        if (reg is not None):  # Some regression can be None if no data was available (all rows in the data contains NaN)
            c = (dict(current_model[0][0]), reg, error, supp, mean_pred, mae, res, list(current_inds), False)

            if reduce_output:
                if msec < (msep * 1.01 if msep > 0 else msec + 1):
                    models.append(c)
                else:
                    error = current[3]
            else:
                models.append(c)

            if (default_model is None) and (c[0] == {}):
                default_model = c
                # break;

            # Check if there is a better parent to the current rule
            # If true, we stop the exploration for this branch
            existBetterParents = exist_better_parents(c, data, work_data, target, tol, error_metric, chosen_reg_type)
            #if existBetterParents :
            #    print('pruning', c[0])

            if not existBetterParents:
                # print('No better parents for', c[0])
                right_a = rightx(current_model[0][1], all_attr)
                loc_frequent_attr = get_local_frequents_val(right_a, symb_attr, current_freq_vals,
                                                            work_data.iloc[current_inds])
                if ignored_sym_conds is not None:
                    for cond in ignored_sym_conds:
                        if cond[0] in loc_frequent_attr:
                            try:
                                loc_frequent_attr[cond[0]].remove(cond)
                            except:
                                pass
                            if len(loc_frequent_attr[cond[0]]) == 0:
                                del loc_frequent_attr[cond[0]]

                # Split between small error and large error
                inds_SE, inds_LE = None, None

                if discretize_method == 'mdlp':
                    inds_SE, inds_LE = split_SE_LE(current_inds, res)
                elif discretize_method == 'mdlp-tv' :
                    inds_SE, inds_LE = split_SV_LV(work_data.iloc[current_inds], target)

                # Discretization part
                vals_inds = None
                if discretize:
                    right_num = [a for a in right_a if a in num_attrs_for_disc]
                    vals_inds, num_feat, maxicv, minicv = discretize_features(right_num, work_data.iloc[current_inds], minsup,
                                                              data_size, target, inds_SE, inds_LE, discretize_method,
                                                              force_discretize)
                    loc_frequent_attr.update(num_feat)

                # Candidate generation
                for a in loc_frequent_attr:
                    for val in loc_frequent_attr[a]:
                        newpattern = current_model[0][0].copy()
                        if val not in newpattern:
                            newpattern.append(val)
                            if a in symb_attr:
                                tid = list(numpy.intersect1d(current_inds, freq_ids[a][val]))
                            else:
                                tid = list(numpy.intersect1d(current_inds, vals_inds[val]))

                            supp = len(tid) / data_size
                            if supp > minsup:
                                t1 = time.time()
                                if closure_num:
                                    closed = closure(tid, vals_inds,
                                                     work_data.iloc[tid][[a for a in symb_attr if a in right_a]],
                                                     all_attr, newpattern)
                                else:
                                    closed = closure(tid, None,
                                                     work_data.iloc[tid][[a for a in symb_attr if a in right_a]],
                                                     all_attr, newpattern)

                                closure_time += time.time() - t1
                                if is_first_parent(closed, newpattern):
                                    stack.append((((closed, a), None, numpy.inf, supp, numpy.nan, numpy.nan),
                                                  loc_frequent_attr, tid, error))

    return models, default_model


def not_covered_indexes(belongs, chosen):
    #tmpb = numpy.array(belongs)
    v = [numpy.where(x == 0)[0] for x in belongs[chosen]]
    if len(v) > 0:
        i = functools.reduce(numpy.intersect1d, v)
    else:
        i = numpy.array([])
    return i


def load_models(model_file):
    f = open(model_file, 'rb')
    models = pickle.load(f)
    num_attrs = pickle.load(f)
    target = pickle.load(f)
    base_num_attrs = pickle.load(f)
    f.close()
    return models, num_attrs, target, base_num_attrs


def predict(models, data, num_quadra, num_lin, query_pattern_fn = pattern_query):
    preds = numpy.full(data.shape[0], numpy.inf)
    divs = numpy.zeros(data.shape[0])
    df = data.dropna(axis=0)
    def_y_preds = None
    for m in models:
        num_attrs = num_quadra if m[8] else num_lin
        if m[0] != {}:
            qr = query_pattern_fn(m[0], num_attrs)
            tmpdf = queryDataset(df, qr)[num_attrs]
            if tmpdf.shape[0] > 0:
                X_values = numpy.array(tmpdf)
                y_pred = m[1].predict(X_values).ravel()
                for i in range(y_pred.size):
                    index = tmpdf.index[i]
                    #v = y_pred[i] if numpy.isinf(preds[index]) else preds[index]+y_pred[i] #((preds[index]+y_pred[i])/2)
                    if numpy.isinf(preds[index]):
                        v = y_pred[i]
                    else:
                        v = preds[index] + y_pred[i]

                    divs[index] = divs[index] + 1
                    preds[index] = v
        else:
            tmpdf = df[num_attrs]        
            X_values = numpy.array(tmpdf)
            def_y_preds = m[1].predict(X_values).ravel()

    ## Look at the uncovered indexes
    inds = numpy.where(numpy.isinf(preds))
    #print('Uncovered data: ', inds)
    #print('Boosting vector', divs)
    divs[inds] = 1
    preds[inds] = def_y_preds[inds]
    preds = preds / divs
    return preds


def predict_with_boosting(models, data, num_quadra, num_lin,
                          query_pattern_fn = pattern_query, outStats=None):
    '''
    @boosting_fn can be either "weighted" or "max". The weights consider the normalized error of the models
    that apply to a data point
    '''
    #df = data.dropna(axis=0).reset_index(drop=True)
    df = data
    preds = numpy.full(df.shape[0], numpy.inf)
    divs = numpy.zeros(df.shape[0])
    counts = numpy.zeros(df.shape[0])
    individualPreds = []
    def_y_preds = None
    for m in models:
        num_attrs = num_quadra if m[8] else num_lin
        individualPredsM = numpy.full(df.shape[0], numpy.inf)
        if m[0] != {}:
            qr = query_pattern_fn(m[0], num_attrs)
            tmpdf = queryDataset(df, qr)[num_attrs]
            if tmpdf.shape[0] > 0:
                X_values = numpy.array(tmpdf)
                y_pred = m[1].predict(X_values).ravel()
                for i in range(y_pred.size):
                    index = tmpdf.index[i]
                    #v = y_pred[i] if numpy.isinf(preds[index]) else preds[index]+y_pred[i] #((preds[index]+y_pred[i])/2)
                    weight = (1/(m[5] + 1.0))
                    if numpy.isinf(preds[index]):
                        v = weight * y_pred[i]
                    else:
                        v = preds[index] + weight * y_pred[i]

                    divs[index] = divs[index] + weight ## Accumulate the error of each regressor
                    counts[index] += 1
                    preds[index] = v
                    individualPredsM[index] = v
        else:
            tmpdf = df[num_attrs]
            X_values = numpy.array(tmpdf)
            def_y_preds = m[1].predict(X_values).ravel()

        individualPreds.append(individualPredsM)


    ## Look at the uncovered indexes
    inds = numpy.where(numpy.isinf(preds))
    divs[inds] = 1
    preds[inds] = def_y_preds[inds]
    individualPredsMDef = numpy.full(df.shape[0], numpy.inf)
    individualPredsMDef[inds] = def_y_preds[inds]
    individualPreds.append(individualPredsMDef)
    preds = preds / divs

    for indPreds in individualPreds :
        mask = indPreds != numpy.inf
        indPreds[mask] = indPreds[mask] / divs[mask]

    if outStats is not None :
        outStats['boosting_rate'] = numpy.average(counts)
        outStats['individual_predictions'] = individualPreds

    return preds


def read_data(data_file):
    data = None
    
    if '.arff' in data_file:
        data = file_utils.read_arff_to_dataframe(data_file)
    elif '.csv' in data_file:
        data = pandas.read_csv(data_file, dtype=attrs_types)
    else:
        print('Unsupported data format, only ARFF and CSV are supported.')
        return
    
    return data


def hipar(train_data, target, minsup, output_file='./r', poly_reg=False, ignored_attrs=None,
                   discretize=True, discretize_method='mdlp',
                   error_metric='rmse', reg_type='omp', reduce_output=True,
                   n_rules=1, support_bias=1.0,
                   closure_num=True, greedy=False, ignored_attrs_for_disc=None,
                    ignored_sym_conds=None, overlap_bias=1.0, force_discretize=False) :
    '''It calls the HIPAR algorithm: Hierarchical Interpretable Pattern-aided regression
       Important parameters
       -----------------------
       train_data : pandas.DataFrame
            The data used to learn the hybrid rules
        minsup: float or int
            Minimum support threshold provided as an absolute number or as a ratio
        error_metric: str
            rmse (by default) or mae (Median Average Error)
        reg_type: str
            omp, lars, dynamic (it makes HIPAR choose the most performing method)
        support_bias: float (>= 0)
            1.0 by default means equal weight for support and coverage. It determines the importance of
            support in the scoring of a rule.
        overlap_bias: float (>=0)
            1.0 by default. It defines the magnitude of the penalty for overlapping rules.
    '''

    mine_num_rules2(train_data, target, minsup, output_file, poly_reg, ignored_attrs,
                   discretize, discretize_method,
                   error_metric, reg_type, reduce_output,
                   n_rules, support_bias,
                   closure_num, greedy, ignored_attrs_for_disc,
                    ignored_sym_conds, overlap_bias, force_discretize)

def mine_num_rules2(train_data, target, minsup, output_file='./r', poly_reg=True, ignored_attrs=None,
                   discretize=True, discretize_method='mdlp',
                   error_metric='rmse', reg_type='omp', reduce_output=True,
                   n_rules=1, support_bias=0.5,
                   closure_num=True, greedy=False, ignored_attrs_for_disc=None,
                    ignored_sym_conds=None, overlap_bias=1.0, force_discretize=False):
    data = train_data.copy()

    symb_attr = list(data.select_dtypes(include='object'))
    num_attr = list(data.select_dtypes(include=['int64', 'float64']))

    if target not in num_attr:
        print('Target is not an numerical attribute.')
        return

    if ignored_attrs is not None:
        if target in ignored_attrs:
            ignored_attrs.remove(target)
    else:
        ignored_attrs = []

    symb_attr = [a for a in symb_attr if a not in ignored_attrs]
    num_attr = [a for a in num_attr if a not in ignored_attrs]

    data = data[(symb_attr + num_attr)]
    num_attr.remove(target)

    data = data.dropna(axis=0)

    poly_vars = None
    if poly_reg:  # Create polynomial features
        data, poly_vars = sparse_linear.data_poly(data, num_attr, target)

    data.reset_index(inplace=True, drop=True)
    data_size = data.shape[0]

    models, default_model = closed_rule_search2(data, target, minsup, num_attr, symb_attr,
                                                   discretize=discretize,
                                                   discretize_method=discretize_method,
                                                   error_metric=error_metric,
                                                   reduce_output=reduce_output, closure_num=closure_num,
                                                   ignored_attrs_for_disc=ignored_attrs_for_disc,
                                                   ignored_sym_conds=ignored_sym_conds,
                                                   force_discretize=force_discretize)

    solutions = None
    belongs = diversity_utils.cover_matrix(models, data)
    if not greedy :
        chosen, solutions = diversity_utils.diversity_mip2(n_rules, models, support_bias=support_bias,
                                                          overlap_bias=overlap_bias)
    else :
        chosen = select_best_models_greedy(models, data, n_rules, support_bias=support_bias)

    has_default_model = False

    chosed_models = []
    for c in chosen:
        tmpm = models[c]
        if tmpm[0] == {}:
            has_default_model = True
        chosed_models.append(tmpm)

    num_attr_poly = list(data.select_dtypes(include=['int64', 'float64']))
    num_attr_poly.remove(target)
    # rules = naive_rules.pretty_rules_html(data, chosed_models, num_attr_poly, poly_vars, target, error_metric)
    rules = naive_rules.pretty_rules(chosed_models, num_attr_poly, poly_vars, target)

    # Learn and add a default model
    if not has_default_model:
        #chosed_models.append(default_model)
        not_covered = not_covered_indexes(belongs, chosen)
        ## Have a look here!!! Something stinks
        if not_covered.size >= int(math.ceil(minsup * len(data))):
            if reg_type == 'dynamic' :
                defreg, error, mean_pred, mae, res, cv_score, chosen_reg_type = sparse_linear.sparse_regression(data.iloc[not_covered],
                                                                                            num_attr, target, 0,
                                                                                            error_metric=error_metric,
                                                                                            cv=2)
            else:
                defreg, error, mean_pred, mae, res, cv_score = sparse_linear.omp_regression(data.iloc[not_covered],
                                                                                        num_attr, target, 0,
                                                                                        error_metric=error_metric,
                                                                                        reg_type=reg_type, cv=None)
            if mae < default_model[5] :
                #print('Left-over rule', mae, default_model[5])
                chosed_models.append(({}, defreg, error, len(not_covered) / len(data), mean_pred, mae, res,
                                      not_covered, False))
            else:
                #print('Default rule added a posteriori because left-over sucks')
                chosed_models.append(default_model)

        else:
            #print('Default rule added a posteriori')
            chosed_models.append(default_model)

#    else :
#        print('Default rule taken')

    rule_file = output_file + '.html'
    model_file = output_file + '.bin'

    # Write rules in a text file
    exout = io.open(rule_file, "w", encoding="utf-8")
    exout.write(rules)
    exout.close()

    # Write the models to make predictions later
    omf = open(model_file, 'wb')
    pickle.dump(chosed_models, omf)
    pickle.dump(num_attr_poly, omf)
    pickle.dump(target, omf)
    pickle.dump(num_attr, omf)
    omf.close()

    return chosed_models


def mine_num_rules_debug(train_data, target, minsup, output_file='./r', poly_reg=True, ignored_attrs=None,
                   discretize=True, discretize_method='mdlp',
                   error_metric='rmse', reg_type='omp', reduce_output=True,
                   n_rules=1, support_bias=0.5,
                   closure_num=True, greedy=False, ignored_attrs_for_disc=None,
                    ignored_sym_conds=None, overlap_bias=1.0, force_discretize=False):
    data = train_data.copy()

    symb_attr = list(data.select_dtypes(include='object'))
    num_attr = list(data.select_dtypes(include=['int64', 'float64']))

    if target not in num_attr:
        print('Target is not an numerical attribute.')
        return

    if ignored_attrs is not None:
        if target in ignored_attrs:
            ignored_attrs.remove(target)
    else:
        ignored_attrs = []

    symb_attr = [a for a in symb_attr if a not in ignored_attrs]
    num_attr = [a for a in num_attr if a not in ignored_attrs]

    data = data[(symb_attr + num_attr)]
    num_attr.remove(target)

    data = data.dropna(axis=0)

    poly_vars = None
    if poly_reg:  # Create polynomial features
        data, poly_vars = sparse_linear.data_poly(data, num_attr, target)

    data.reset_index(inplace=True, drop=True)
    data_size = data.shape[0]

    t = time.process_time()
    models, default_model = closed_rule_search2(data, target, minsup, num_attr, symb_attr,
                                                   discretize=discretize,
                                                   discretize_method=discretize_method,
                                                   error_metric=error_metric,
                                                   reduce_output=reduce_output, closure_num=closure_num,
                                                   ignored_attrs_for_disc=ignored_attrs_for_disc,
                                                   ignored_sym_conds=ignored_sym_conds,
                                                   force_discretize=force_discretize)
    time_exploration = time.process_time() - t
    solutions = None
    belongs = diversity_utils.cover_matrix(models, data)
    t = time.process_time()
    if not greedy :
        chosen, solutions = diversity_utils.diversity_mip2(n_rules, models, support_bias=support_bias,
                                                          overlap_bias=overlap_bias)
    else :
        chosen = select_best_models_greedy(models, data, n_rules, support_bias=support_bias)

    has_default_model = False
    time_selection = time.process_time() - t

    chosed_models = []
    for c in chosen:
        tmpm = models[c]
        if tmpm[0] == {}:
            has_default_model = True
        chosed_models.append(tmpm)

    num_attr_poly = list(data.select_dtypes(include=['int64', 'float64']))
    num_attr_poly.remove(target)
    # rules = naive_rules.pretty_rules_html(data, chosed_models, num_attr_poly, poly_vars, target, error_metric)
    rules = naive_rules.pretty_rules(chosed_models, num_attr_poly, poly_vars, target)

    # Learn and add a default model
    if not has_default_model:
        #chosed_models.append(default_model)
        not_covered = not_covered_indexes(belongs, chosen)
        ## Have a look here!!! Something stinks
        if not_covered.size >= int(math.ceil(minsup * len(data))):
            if reg_type == 'dynamic' :
                defreg, error, mean_pred, mae, res, cv_score, chosen_reg_type = sparse_linear.sparse_regression(data.iloc[not_covered],
                                                                                            num_attr, target, 0,
                                                                                            error_metric=error_metric,
                                                                                            cv=2)
            else:
                defreg, error, mean_pred, mae, res, cv_score = sparse_linear.omp_regression(data.iloc[not_covered],
                                                                                        num_attr, target, 0,
                                                                                        error_metric=error_metric,
                                                                                        reg_type=reg_type, cv=None)
            if mae < default_model[5] :
                #print('Left-over rule', mae, default_model[5])
                chosed_models.append(({}, defreg, error, len(not_covered) / len(data), mean_pred, mae, res,
                                      not_covered, False))
            else:
                #print('Default rule added a posteriori because left-over sucks')
                chosed_models.append(default_model)

        else:
            #print('Default rule added a posteriori')
            chosed_models.append(default_model)

#    else :
#        print('Default rule taken')

    rule_file = output_file + '.html'
    model_file = output_file + '.bin'

    # Write rules in a text file
    exout = io.open(rule_file, "w", encoding="utf-8")
    exout.write(rules)
    exout.close()

    # Write the models to make predictions later
    omf = open(model_file, 'wb')
    pickle.dump(chosed_models, omf)
    pickle.dump(num_attr_poly, omf)
    pickle.dump(target, omf)
    pickle.dump(num_attr, omf)
    omf.close()

    return chosed_models, {'exploration-time' : time_exploration, 'selection-time' : time_selection}



def cross_validation_hr(input_data, cv, parameters, seed=42, boosting="avg", greedy=False, force_discretize=False):
    splits = []
    frac = 1 / cv
    
    data = input_data.copy()
    data = data.sample(frac=1, random_state=seed)
    
    split_size = int(data.shape[0] * frac)
    
    stats = {}
    number_of_rules = []
    number_of_coefficients = []
    number_of_conditions = []
    
    for i in range(cv):
        #test = data.sample(frac=frac, random_state=seed)
        test = data.iloc[i*split_size:i*split_size+split_size]
        train_inds = [i for i in data.index if i not in test.index]
        s = (train_inds, list(test.index))
        splits.append(s)
        
    scores_rmse = numpy.full(cv, numpy.inf, dtype=numpy.float64)
    scores_mae = numpy.full(cv, numpy.inf, dtype=numpy.float64)
    i = 0
    for s in splits:
        print('Round', i)
        train = data.iloc[s[0]].reset_index(drop=True)
        test = data.iloc[s[1]].reset_index(drop=True)
        mine_num_rules2(train, parameters['target'], parameters['minsup'],
               output_file=parameters['output_file'], poly_reg=parameters['poly_reg'], ignored_attrs=parameters['ignored_attrs'], 
               discretize=parameters['discretize'], discretize_method=parameters['discretize_method'], 
               error_metric=parameters['error_metric'],
                        reg_type=parameters['reg_type'],
               reduce_output=parameters['reduce_output'], n_rules=parameters['n_rules'],
               support_bias=parameters['support_bias'],
               closure_num=parameters['closure_num'], greedy=greedy, overlap_bias=parameters['overlap_bias'],
                        force_discretize=force_discretize)
        models, num_attrs, target, base_num_attrs = load_models(parameters['output_file'] + '.bin')
        test_poly, poly_vars = sparse_linear.data_poly(test, base_num_attrs, target)
        if boosting == 'weighted' :
            boostingStats = {}
            y_preds = predict_with_boosting(models, test_poly, num_attrs, base_num_attrs, pattern_query, boostingStats)
            stats['boosting_rate'] = boostingStats['boosting_rate']
        else:
            y_preds = predict(models, test_poly, num_attrs, base_num_attrs)

        y = numpy.array(test[parameters['target']])
        number_of_rules.append(len(models))
        number_of_coefficients.extend([len(m[1].coef_) for m in models])
        number_of_conditions.extend([len(m[0]) for m in models])

        #print('Error', numpy.abs(y - y_preds))
        rmse = numpy.sqrt(metrics.mean_squared_error(y, y_preds))
        mae = metrics.median_absolute_error(y, y_preds)
        
        scores_rmse[i] = rmse
        scores_mae[i] = mae
        i += 1
    
    stats['num_rules'] = numpy.mean(number_of_rules)
    stats['num_coefficients'] = numpy.mean(number_of_coefficients)
    stats['num_conditions'] = numpy.mean(number_of_conditions)
    return scores_rmse, scores_mae, splits, stats


def split_data(input_data, cv, seed=42, num_rules=1):
    splits = []
    frac = 1 / cv

    data = input_data.copy()
    data = data.sample(frac=1, random_state=seed)

    split_size = int(data.shape[0] * frac)

    stats = {}
    number_of_rules = []
    number_of_coefficients = []
    number_of_conditions = []

    for i in range(cv):
        test = data.iloc[i * split_size:i * split_size + split_size]
        train_inds = [i for i in data.index if i not in test.index]
        s = (train_inds, list(test.index))
        splits.append(s)

    stats['num_rules'] = num_rules
    return 0.0, 0.0, splits, stats

#if __name__ == "__main__":
 #   main()
