import numpy
import pandas
import math

from scipy.sparse import csr_matrix
from functools import reduce
from numba import jit

from ortools.linear_solver import pywraplp
from ortools.constraint_solver import pywrapcp

from sklearn.preprocessing import MinMaxScaler


# Will probably be moved into a new python file to avoid redundant code
def num_query(num_cond):
    query = ''
    for k in num_cond:
        query += k + num_cond[k] + ' and '
    query = query[:-5]
    if 'nan' in query:
        query = ''
    return query

def get_indexes(data, pattern):
    inds = []
    tmpdf = data.copy()
    
    num_attr = tmpdf.select_dtypes(include='float64')
    symb_attr = tmpdf.select_dtypes(include='object')
    
    symb_cond = {k:pattern[k] for k in symb_attr if k in pattern}
    num_cond = {k:pattern[k] for k in num_attr if k in pattern}
    
    if len(symb_cond) > 0:
        tmpdf = tmpdf.loc[(tmpdf[list(symb_cond)] == pandas.Series(symb_cond)).all(axis=1)]
    
    if len(num_cond) > 0:
        query = num_query(num_cond)
        if len(query) > 0:
            tmpdf = tmpdf.query(query)
            
    return tmpdf.index


def error_matrix(models, data, target):
    ma = []
    preds = []
    s = data.shape[0]
    
    for m in models:
        pattern = m[0]
        errors = []
        tma = None
        
        if len(pattern) != 0:
            tma = list(get_indexes(data, pattern))
            ma.append(tma)
        else:
            tma = list(data.index)
            ma.append(list(tma))
        
        #npred = numpy.full(s, -1, dtype=float)
        
        #if m[1] is not None:
        #    tdf = data.iloc[tma]
        #    tdf = tdf.select_dtypes(include=['float64'])
        #    y = numpy.array(tdf[target])            
        #    tdf = tdf.drop(target, axis=1)
        #    #tdf = tdf.dropna(axis=0)
        #    
        #    if tdf.shape[0] > 0:
        #        X = numpy.array(tdf)
        #        pred = m[1].predict(X)
        #
        #        tdf = tdf.reset_index()
        #                    
        #        inds = list(tdf.index)
        #        res = y[inds] - pred[inds]
        #        npred[tma] = res
        
        #preds.append(npred)
        
    return ma #, preds
            

    
'''def cover_matrix(models, data, target):
    ndp = data.shape[0]
    ma = error_matrix(models, data, target)
    ba = []
    
    for l in ma:
        tba = numpy.zeros(ndp, dtype=int)
        tba[l] = 1        
        ba.append(list(tba))
    
    return ba #, preds 
'''

def cover_matrix(models, data):
    ndp = data.shape[0]
    ba = []
    
    for m in models:
        tba = numpy.zeros(ndp, dtype=int)
        tba[m[7]] = 1
        ba.append(list(tba))
    
    ba = numpy.array(ba)
    #ba = csr_matrix(ba)
    return ba



'''def compute_weights(num_rules, error_matrix):
    average_error_per_rule = numpy.full(num_rules, 0.0, dtype=numpy.float64)
    
    for i in range(num_rules) :        
        rule_error = [e for e in error_matrix[i] if e > -1]
        if len(rule_error) > 0:
            average_error_per_rule[i] = numpy.mean(rule_error)
        else:
            average_error_per_rule[i] = 0
        #average_error_per_rule[i] = numpy.mean(error_matrix[i][numpy.sign(error_matrix[i]) >= 0])
        
    
    #nozeroerr = [average_error_per_rule[i] for i in range(len(average_error_per_rule)) if average_error_per_rule[i] > 0]
    nozeroerr = average_error_per_rule[average_error_per_rule != 0.0]
    min_error = numpy.min(nozeroerr)
    average_error_per_rule[average_error_per_rule == 0.0] = min_error
    
    scale_factor = 1.0
    if min_error < 1.0 :
        scale_factor = abs(math.floor(math.log10(min_error)))
        
    weights = numpy.rint(average_error_per_rule * pow(2, scale_factor)) 
    weights = weights.astype(int)
       
    return weights'''

@jit
def compute_jaccard(models):
    jaccard_matrix = numpy.full((len(models),len(models)), fill_value=-1, dtype=float)
    
    for i1 in range(len(models)):
        for i2 in range(len(models)):
            if i1 != i2:
                intersect_size = numpy.intersect1d(models[i1][7], models[i2][7]).size
                j = intersect_size / (len(models[i1][7]) + len(models[i2][7]) - intersect_size)
                jaccard_matrix[i1][i2], jaccard_matrix[i2][i1] = j, j
        
    return jaccard_matrix
        

'''def normalize(v):
    norm = numpy.linalg.norm(v)
    if norm == 0: 
        return v
    return v / norm'''

def normalize(v):
    minv = min(v)
    maxv = max(v)
    minmax = 1 if (max(v) == min(v)) else (maxv-minv)
    return (v-minv)/minmax
    
    
def normalize2(v):
    n = sum(v)
    if n > 0 :
        return v/n
    else:
        return v


'''def minmaxscaler(v):
    X_std = (v - v.min()) / (v.max() - v.min())
    X_scaled = X_std * (v.max() - v.min()) + v.min()
    return X_scaled'''

def minmaxscaler(v):
    X_std = (v - v.min()) / (v.max() - v.min())
    return X_std

def diversity_mip(k, models, data_size, cover_all=False, overlap_constraint='jaccard', support_bias=0.5, strict_overlap=False, 
                  belongs=None, no_k=False, max_prediction=False, overlap_bias=1.0):

    if no_k and not cover_all:
        cover_all = True

    num_rules = len(models)    
    errors = numpy.array([m[5] for m in models])
    #print(errors)
    supports = numpy.array([m[3] for m in models])

    
    errors = numpy.clip(normalize2(errors), 0.005, 1)
    supports = normalize2(supports)
    #print('N', errors)


    if not numpy.all(numpy.isnan(errors)) :
        minerror = min([e for e in errors if (e != numpy.nan and e > 0)])
        if minerror == 0.0 :
            minerror = 0.005
    else :
        print(models)
        print(errors)
        minerror = 0.005

    
    solver = pywraplp.Solver('DiversityMIP', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)

    x = [solver.IntVar(0, 1, "x[%i]" % i) for i in range(num_rules)]
    
    jaccard_matrix = None
    if overlap_constraint == 'jaccard':
        jaccard_matrix = compute_jaccard(models)
        xrs = {}
        for i in range(num_rules):
            for j in range(num_rules):
                xrs[i,j] = solver.NumVar(0, 1, "x[%i,%i]" % (i,j))


    if overlap_constraint=='hack' or cover_all:
        max_cover = max(2,int(k/10)) if not strict_overlap else 1
        for bt in belongs.T:
            cov_constraint = solver.Constraint((1 if cover_all else 0), (max_cover if overlap_constraint else solver.infinity()))
            for i in range(len(bt)):
                cov_constraint.SetCoefficient(x[i], float(bt[i]))
    elif overlap_constraint == 'jaccard':
        for i in range(num_rules):
            for j in range(num_rules):
                jaccard_constraint = solver.Constraint(0, 1)
                jaccard_constraint.SetCoefficient(x[i], 1)
                jaccard_constraint.SetCoefficient(x[j], 1)
                jaccard_constraint.SetCoefficient(xrs[i,j], -2)
    
    if not no_k:
        if not max_prediction:
            mink = solver.Constraint(k, solver.infinity())
            for i in range(num_rules):
                mink.SetCoefficient(x[i], 1)
        else:
            maxk = solver.Constraint(0, k)
            for i in range(num_rules):
                maxk.SetCoefficient(x[i], 1)
    
    if not max_prediction:  
        if cover_all:        
            o = solver.Objective()
            for i in range(num_rules):
                o.SetCoefficient(x[i], (errors[i] if errors[i] > 0 else minerror))
        else:
            o = solver.Objective()
            for i in range(num_rules):
                tmpe = errors[i] if not numpy.isnan(errors[i]) and errors[i] > 0.0 else minerror
                tmps = pow(supports[i], support_bias)
                coef = tmpe / tmps
                #print('coef', coef, tmpe, tmps)
                print(x[i], models[i][0], coef)
                o.SetCoefficient(x[i], coef)
                #print('coef', i, coef)
                if overlap_constraint == 'jaccard':
                    for j in range(num_rules):
                        #print('coef jaccard', i, j, jaccard_matrix[i, j], jaccard_matrix[i,j] * overlap_bias)
                        o.SetCoefficient(xrs[i,j], jaccard_matrix[i,j] * overlap_bias)
                        #print(xrs[i, j], jaccard_matrix[i,j] * overlap_bias)

        o.SetMinimization()
    else:
        preds = numpy.array([m[4] for m in models])
        o = solver.Objective()
        for i in range(num_rules):
            o.SetCoefficient(x[i], preds[i])
        
        o.SetMaximization()
        
    solver.SetTimeLimit(int(5 * 1000 * 60))
    solver.Solve()
    
    choosen = [i for i in range(len(x)) if x[i].solution_value() == 1]
    print(choosen)
    sol = [e.solution_value() for e in x]

    return choosen, sol


def diversity_mip2(k, models, support_bias=0.5, overlap_bias=1.0):
    num_rules = len(models)
    MINERROR = 0.0000001

    if num_rules == 0 :
        return None, None

    if num_rules == 1 :
        return [0], 1

    errors = numpy.array([m[5] for m in models])
    #print(errors)
    supports = numpy.array([m[3] for m in models])

    errors = numpy.clip(normalize2(errors), MINERROR, 1)
    supports = normalize2(supports)

    if not numpy.all(numpy.isnan(errors)):
        minerror = min([e for e in errors if (e != numpy.nan and e > 0)])
        if minerror == 0.0:
            minerror = MINERROR
    else:
        minerror = MINERROR

    solver = pywraplp.Solver('DiversityMIP', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)

    x = [solver.IntVar(0, 1, "x[%i]" % i) for i in range(num_rules)]

    jaccard_matrix = compute_jaccard(models)
    xrs = {}
    for i in range(num_rules):
        for j in range(i + 1, num_rules):
            xrs[i, j] = solver.NumVar(0, 1, "x[%i,%i]" % (i, j))

    for i in range(num_rules):
        for j in range(i + 1, num_rules):
            jaccard_constraint = solver.Constraint(0, 1)
            jaccard_constraint.SetCoefficient(x[i], 1)
            jaccard_constraint.SetCoefficient(x[j], 1)
            jaccard_constraint.SetCoefficient(xrs[i, j], -2)

    mink = solver.Constraint(k, solver.infinity())
    for i in range(num_rules):
        mink.SetCoefficient(x[i], 1)


    o = solver.Objective()
    coefsList = []
    for i in range(num_rules):
        tmpe = errors[i] if not numpy.isnan(errors[i]) and errors[i] > 0.0 else minerror
        tmps = pow(supports[i], support_bias)
        coef = -tmps / tmpe
        coefsList.append(abs(coef))
        #print(x[i], models[i][0], coef, 'raw support:', supports[i], 'new support:', tmps, 'error:', tmpe)
        o.SetCoefficient(x[i], coef)

    #print('ob=', overlap_bias)
    for i in range(num_rules) :
        #coefstr = ''
        for j in range(i + 1, num_rules):
            coefij = (jaccard_matrix[i, j] * overlap_bias) * (coefsList[i] + coefsList[j])
            o.SetCoefficient(xrs[i, j], coefij)
            #coefstr = coefstr + '[' + str(xrs[i, j]) + '--' + str(coefij) + '--' + str(jaccard_matrix[i, j]) + '],  '
        #print(coefstr)

    o.SetMinimization()
    solver.SetTimeLimit(int(5 * 1000 * 60))
    solver.Solve()

    chosen = [i for i in range(len(x)) if x[i].solution_value() == 1]
    #print('chosen', chosen)
    sol = [e.solution_value() for e in x]

    return chosen, sol

@jit
def maxoutput(models, k):    
    a = []
    for i in range(len(models)):
        v = (i, models[i][4])
        a.append(v)

    dtype = [('index', int), ('prediction', float)]
    
    a = numpy.array(a, dtype=dtype)
    asort = numpy.sort(a, order='prediction') 
    
    inds = [i[0] for i in a[k:]]
    return inds