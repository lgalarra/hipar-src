import pandas
import numpy

from sklearn import linear_model
from sklearn import metrics
from sklearn import preprocessing
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import GradientBoostingRegressor

import time



'''def normalize_vect(v):
    norm = numpy.linalg.norm(v)
    if norm == 0: 
        return v
    return v / norm'''

def normalize_vect(v):
    minv = min(v)
    maxv = max(v)
    minmax = 1 if (max(v) == min(v)) else (maxv-minv)
    return (v-minv)/minmax

def normalize_matrix(m):
    #return preprocessing.normalize(m, norm='l2', axis=0)
    min_max_scaler = preprocessing.MinMaxScaler()
    return min_max_scaler.fit_transform(m)


"""Find (with scikit learn) linear regression parameters
Return the regression model, the mean squared error, the real values and the predicted values"""
def find_linear_regression(df: pandas.DataFrame, dependant_attr, independant_attrs: list, polynomial=False, degree=2, 
                           method=None, alpha=1, nonzero=3):
    #t1 = time.time()
    
    if method is None:
        reg = linear_model.LinearRegression(n_jobs=1)
    elif method == 'lasso':
        reg = linear_model.Lasso(alpha=alpha)
    elif method == 'omp':
        reg = linear_model.OrthogonalMatchingPursuit(n_nonzero_coefs=nonzero, fit_intercept=True, normalize=True)
    elif method == 'ompcv':
        mit = max(int(len(independant_attrs)*(1/30)), 1)
        reg = linear_model.OrthogonalMatchingPursuitCV(max_iter=mit, cv=4, n_jobs=1)
    elif method == 'gbr' :
        reg = GradientBoostingRegressor(n_estimators=1000, min_samples_leaf=0.02)
    else:
        print("Unknown regression method")
        return
    
    shape = df.shape
    
    if shape[0] == 0:
        return None, None, None, None
    
    tmpX = df[independant_attrs]
    tmpy = df[dependant_attr]
    X_values = numpy.matrix(tmpX)
    y_values = numpy.array(tmpy)
    
    if polynomial:
        poly = preprocessing.PolynomialFeatures(degree=degree, interaction_only=True)
        X_values = poly.fit_transform(X_values)
    
    X_values = normalize_matrix(X_values)
    y_values = normalize_vect(y_values)
    
    #t2 = time.time()
    
    try:
        reg.fit(X_values, y_values)   
    except ValueError:
        #print("ValueError !")
        return None, None, None, None
    
    #t3 = time.time()
    
    y_pred = reg.predict(X_values)    
    mse = numpy.sqrt(metrics.mean_squared_error(y_values, y_pred))
    
    #t4 = time.time()
    
    #fit_time = t3 - t2
    #other_time = (t4 - t3) + (t2 - t1)
    
    #print("Fit time : " + str(fit_time) + "\nOther time : " + str(other_time))
    
    return reg, mse, y_values, y_pred 



def balance_target(data, target):
    indexes = []
    data_size = data.shape[0]
    nzero = data[data[target] == 0.0].shape[0]
    n_nonzero = data_size - nzero
    if nzero > (data_size/2):
        tmpdf = data.sample(frac=1)
        tmpdf = tmpdf[tmpdf[target] == 0].iloc[:n_nonzero]
        indexes = indexes + list(tmpdf.index)
        
    indexes = indexes + list(data[data[target] != 0.0].index)
    return sorted(indexes)


def augment_data(data, target):
    rdata = data.copy()
    data_size = rdata.shape[0]
    nzero = rdata[rdata[target] == 0.0].shape[0]
    n_nonzero = data_size - nzero
    if (nzero > n_nonzero > 0):
        toadd = nzero - n_nonzero
        tmpdf = rdata[rdata[target] != 0].sample(frac=1)
        tmpdf = tmpdf.iloc[:toadd]
        rdata = rdata.append(tmpdf, ignore_index=True)
    return rdata


def sparse_regression(df: pandas.DataFrame, num_attr, target, tol, error_metric='rmse', cv=None) :
    reg_lars, l2error_lars, mean_pred_lars, mae_lars, res_lars, cv_score_lars = omp_regression(
        df, num_attr, target, tol, error_metric=error_metric, reg_type='lars', cv=cv)

    reg_omp, l2error_omp, mean_pred_omp, mae_omp, res_omp, cv_score_omp = omp_regression(
        df, num_attr, target, tol, error_metric=error_metric, reg_type='omp', cv=cv)

    # We chose the best model based on the lowest error
    if cv_score_lars < cv_score_omp:
        reg_type = 'lars'
        return reg_lars, l2error_lars, mean_pred_lars, mae_lars, res_lars, cv_score_lars, reg_type
    else:
        reg_type = 'omp'
        return reg_omp, l2error_omp, mean_pred_omp, mae_omp, res_omp, cv_score_omp, reg_type


def omp_regression(df: pandas.DataFrame, num_attr, target, tol, error_metric='rmse', reg_type='omp', cv=None):
    tmpdf = df.copy().reset_index(drop=True)
    
    true_X_values = numpy.array(tmpdf[num_attr])
    true_y_values = numpy.array(tmpdf[target])
        
    if tmpdf.shape[0] == 0:
        return None, None, None, None, None, None

    tmpX = tmpdf[num_attr]
    tmpy = tmpdf[target]
    X_values = numpy.array(tmpX)
    y_values = numpy.array(tmpy)

    from sklearn.model_selection import train_test_split
    if len(X_values) >= 10 :
        X_train, X_test, y_train, y_test = train_test_split(X_values, y_values, test_size = 0.2, random_state = 42)
    else :
        X_train = X_values
        y_train = y_values
        X_test = X_values
        y_test = y_values
    
    if reg_type == 'omp':
        reg = linear_model.OrthogonalMatchingPursuit(n_nonzero_coefs=(3 if tol is None else None), tol=tol,
                                                     fit_intercept=True, normalize=True)
    elif reg_type == 'lars':
        reg = linear_model.LassoLars(alpha=.1, fit_path=False)
    elif reg_type == 'ompcv':
        reg = linear_model.OrthogonalMatchingPursuitCV()
    elif reg_type == 'lin':
        reg = linear_model.LinearRegression()
    elif reg_type == 'gbr' :
        reg = GradientBoostingRegressor(min_samples_leaf=0.02, n_estimators=1000)
    else:
        raise ValueError('Unknown regression type : ' + reg_type)
    
    scores = None
    if cv is not None and len(X_values) >= 10:
        score_metric = 'neg_median_absolute_error' if error_metric=='mae' else 'neg_mean_squared_error'
        cv_scores = abs(cross_val_score(reg, X_values, y_values, cv=min(tmpdf.shape[0], cv), scoring=score_metric,
                                        n_jobs=1))
        if error_metric == 'mae' :
            scores = numpy.median(cv_scores) if numpy.any(cv_scores) else 0
        else :
            scores = numpy.mean(cv_scores) if numpy.any(cv_scores) else 0
        #scores = (pow(numpy.std(cv_scores), 2) / numpy.mean(cv_scores)) if numpy.any(cv_scores) else 0

    #reg.fit(X_values, y_values)
    reg.fit(X_train, y_train)
    #y_pred = reg.predict(true_X_values)
    y_pred = reg.predict(X_test)
    y_pred = y_pred.ravel()
    
    res = numpy.zeros(y_pred.size, dtype=float)
    #res = true_y_values - y_pred
    res = y_test - y_pred
    ares = abs(res)
    #mae = metrics.median_absolute_error(true_y_values, y_pred)
    mae = metrics.median_absolute_error(y_test, y_pred)

    res = numpy.square(res)
    l2sqr = numpy.sum(res)
    mean_pred = numpy.mean(y_pred)
    #rmse = numpy.sqrt(metrics.mean_squared_error(true_y_values, y_pred))
    rmse = numpy.sqrt(metrics.mean_squared_error(y_test, y_pred))
    error = mae if error_metric == 'mae' else rmse

    if scores is None :
        scores = error

    return reg, l2sqr, mean_pred, error, ares, scores


def poly_variables(powers, independant_vars):  
    polyvars = []
    for i in range(len(powers)):
        v = ''
        for j in range(len(powers[i])):
            p = powers[i][j]
            if p > 1:
                v += independant_vars[j] + '^' + str(p) + '*'
            elif p == 1:
                v += independant_vars[j] + '*'
            elif p == 0:
                v += '1*'
        
        v = v[:-1]
        v = v.replace('*1', '')
        v = v.replace('1*', '')
        polyvars.append(v)
    
    return polyvars


def data_poly(data, num_attrs, target, degree=2):
    tmp_attrs = [a for a in num_attrs if a != target]
    new_data = data.select_dtypes(include='object')
    X_values = numpy.array(data[tmp_attrs])
    poly = preprocessing.PolynomialFeatures(degree=degree, interaction_only=False)
    X_values = poly.fit_transform(X_values)
    poly_vars = poly_variables(poly.powers_, tmp_attrs)
    for i in range(len(poly_vars)):
        col_name = poly_vars[i]
        col_vals = X_values.T[i]
        args = {col_name : col_vals}
        new_data = new_data.assign(**args)
    
    t = {target : data[target]}
    new_data = new_data.assign(**t)
    return new_data, poly_vars




#=============================================================================================================================
# Sparse linear regression

#Return, from an attributes list, the list of attributes that are numeric
def get_num_attr(attributes):
    num_attr = []
    for e in attributes:
        if attributes[e] == "numeric":
            num_attr.append(e)
    
    return num_attr
  
    
    
#Create a beam of certain size
def init_beam(beam_size):
    max_float = numpy.finfo(numpy.float64).max
    beam = []
    default = ([], max_float)
    for i in range(beam_size):
        beam.append(default)
    
    return beam        



#Return the best model stored in a beam
def get_beam_best(beam):
    best = beam[0]
    for i in range(1, len(beam)):
        e = beam[i]
        if e[1] < best[1]:
            best = e
    
    return best



#Return the worst model stored in a beam
def get_beam_worst(beam):
    worst = beam[0]
    for i in range(1, len(beam)):
        e = beam[i]
        if e[1] > worst[1]:
            worst = e
    
    return worst
      
    
    
#Merge a beam with a list of candidates by only keeping the bests one
def merge_beam_candidates(beam, candidates):
    newbeam = beam.copy()
    for c in candidates:
        worst = get_beam_worst(newbeam)
        if c[1] <= worst[1]:
            newbeam.remove(worst)
            newbeam.append(c)
    
    return newbeam



#Find the best candidate variable to improve the given model (var_list)
#Return a candidate for merging in the beam
def add_variable(df: pandas.DataFrame, beam, var_list, target, num_attributes, tested, polynomial=False, degree=2):
    
    cp_num_attributes = num_attributes.copy()
    
    tested_cp = tested.copy()
    
    worst = get_beam_worst(beam)
    best = worst[1]
    used_attr = None
    
    candidate = None
    
    pruned = 0
    
    for a in num_attributes:        
        
        tvarl = var_list.copy()       
        
        if a != target:
            tvarl.append(a)
            
            if not tvarl in tested :
                tested_cp.append(tvarl)
                model,error,yval,ypred = find_linear_regression(df, target, tvarl, polynomial, degree)
                if error <= best:
                    best = error
                    newvars = tvarl.copy()
                    newerror = error
                    candidate = (newvars, newerror)                
                    used_attr = a
            else:
                pruned += 1
    
    if used_attr is not None:
        cp_num_attributes.remove(used_attr)
    
    return candidate, cp_num_attributes, tested_cp, pruned
    
    
    
#Main function of the automatic linear regression process
#Manage the beam search
def automatic_linear(df: pandas.DataFrame, target_attribute, beam_size, max_step, polynomial=False, degree=2):

    beam = init_beam(beam_size)
    
    tmpdf = df.select_dtypes(include=['float64'])
    #df = df.drop(target_attribute, axis=1)
    
    best = None
    
    i=0
    error = numpy.finfo(numpy.float64).max
    improvement = error
    
    mintarget = tmpdf[target_attribute].min()
    maxtarget = tmpdf[target_attribute].max()
    min_error = (maxtarget - mintarget)/100
    min_improvement = min_error/4
    
    print("Error min : " + str(min_error) + ", Improvement min : " + str(min_improvement))
    
    pruned = 0
    
    it_attr = list(tmpdf)
    it_attr.remove(target_attribute)
    
    while (i < max_step) and (error > min_error) and (improvement > min_improvement):
        candidates = []
        tested = []
        for j in range(beam_size):
            model = beam[j]
            
            if i == 0: #Ugly : need to find a better way to do it
                candidate, it_attr, tested, p = add_variable(tmpdf, beam, model[0], target_attribute, it_attr, tested, polynomial, degree)
                pruned += p
            else:
                candidate, void, tested, p = add_variable(tmpdf, beam, model[0], target_attribute, it_attr, tested, polynomial, degree)
                pruned += p
            
            if candidate is not None:
                candidates.append(candidate)
        
        beam = merge_beam_candidates(beam, candidates)
        
        best = get_beam_best(beam)
        improvement = error - best[1]
        error = best[1]
        #print("Error = ", error)
        #print("Improvement = ", improvement)
        i += 1   
    
    #print("Number double pruned : " + str(pruned))
    return best





def auto_omp(df: pandas.DataFrame, target, max_error_aug=0.1, return_error_ratio=False):
    tmpdf = df.select_dtypes(include=['float64'])
    
    #tmpdf = tmpdf.dropna(axis=0)
    
    scaler = preprocessing.MinMaxScaler() 
    scaled_values = scaler.fit_transform(tmpdf) 
    tmpdf.loc[:,:] = scaled_values
    
    #print(tmpdf[target].mean())
    
    num_attr = list(tmpdf)
    num_attr.remove(target)
        
    #print(tmpdf)
    
    i = len(num_attr)# - 1
    model,error,yval,ypred = find_linear_regression(tmpdf, target, num_attr, method='omp', nonzero=i)
    
    first_error = max(error, 0.00001)
    print("First error : " + str(first_error))
    stop_cond = False
    
    nvars = []
    erat = []
    
    i -= 1
    while (i > 0) and (not stop_cond):
        #print(i)
        model,error,yval,ypred = find_linear_regression(tmpdf, target, num_attr, method='omp', nonzero=i)
        error = max(error, 0.00001)
        error_ratio = (error/first_error) - 1
        nvars.append(i)
        erat.append(error_ratio*100)
        #print("It " + str(i) + " error ratio : " + str(error_ratio))
        if error_ratio >= max_error_aug:
            stop_cond = True
        i -= 1
        
    print("Error : " + str(error))
    
    if return_error_ratio:
        return model, nvars, erat
    else:
        return model
    
    

def auto_omp_dicho(df: pandas.DataFrame, target, max_error_aug=0.1):
    tmpdf = df.select_dtypes(include=['float64'])
    
    num_attr = list(tmpdf)
    num_attr.remove(target)
    
    maxi = len(num_attr)
    i = int(maxi/8)
    model,error,yval,ypred = find_linear_regression(tmpdf, target, num_attr, method='omp', nonzero=i)
    
    if error is None:
        return None, None
    
    first_error = error
    #first_error = max(error, 1)

    #print(first_error)
    stop_cond = False
    
    i = int(i / 2)
    while (i > 0) and (i < maxi):
        #print(i)
        model,error,yval,ypred = find_linear_regression(tmpdf, target, num_attr, method='omp', nonzero=i)
        #error = max(error, 1)
        error_ratio = numpy.float64((error/first_error) - 1)
        #print("It " + str(i) + " error ratio : " + str(error_ratio))
        #print("It " + str(i) + " : " + str(error))
        if error_ratio >= max_error_aug:
            step = (maxi - i) / 2
        else:
            step = 0 - i/2
            maxi = i
        
        if step == -0.5:
            step = -1
        elif step == 0.5:
            step = 1
        
        i = i + int(step)
    
    #print("Error : " + str(error))
    return model, error