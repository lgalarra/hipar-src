import numpy

def interclass_variance(indexesInClass, indexesNotInClass, globalMean) :
    miuInClass = numpy.mean(indexesInClass)
    miuNotInClass = numpy.mean(indexesNotInClass)
    return miuNotInClass, len(indexesInClass), len(indexesInClass) * pow(miuInClass - globalMean, 2) + \
           len(indexesNotInClass) * pow(miuNotInClass - globalMean, 2)


def recall(laws, models):
    recall_matrix = numpy.zeros((len(laws), len(models)-1), dtype=float)
    for im in range(len(models)):
        if models[im][0] != {}:
            for il in range(len(laws)):
                r = len(numpy.intersect1d(models[im][7], laws[il]['indexes']))/len(laws[il]['indexes'])
                recall_matrix[il][im] = r
            
    return recall_matrix


def recall_cpxr(laws, models_indexes):
    recall_matrix = numpy.zeros((len(laws), len(models_indexes)-1), dtype=float)
    for i in range(len(models_indexes)-1):
        k = list(models_indexes)[i]
        for il in range(len(laws)):
            r = len(numpy.intersect1d(models_indexes[k], laws[il]['indexes']))/len(laws[il]['indexes'])
            recall_matrix[il][i] = r
    
    return recall_matrix


def normalize(v):
    norm = numpy.linalg.norm(v)
    if norm == 0: 
        return v
    return v / norm


def compare_linreg(coefs_vectors1, coefs_vectors2):
    l2_1 = numpy.sqrt(sum(pow(coefs_vectors1,2)))
    l2_2 = numpy.sqrt(sum(pow(coefs_vectors2,2)))
    costeta = l2_1 / l2_2
    return costeta


def vector_angle(v1, v2):
    v1_u = normalize(v1)
    v2_u = normalize(v2)
    return numpy.arccos(numpy.clip(numpy.dot(v1_u, v2_u), -1.0, 1.0))


def law_compare(recall_matrix, models, laws):
    compare_results = []
    for i in range(recall_matrix.shape[0]):
        am = numpy.argmax(recall_matrix[i])
        if (am.dtype == numpy.int64):
            rule_coef_vec = numpy.array([models[am][1].intercept_] + list(models[am][1].coef_))
            true_coef_vec = numpy.array([laws[i]['intercept']] + laws[i]['coefs_vec'])
            angle = vector_angle(true_coef_vec, rule_coef_vec)
            #dist_angle = round(abs(1 - angle),2)
            compare_results.append((am, recall_matrix[i][am], angle))
        else:
            print() #several rules cover the larger part of the original law
            # this should never happen
    return sorted(compare_results)


def law_compare_cpxr(recall_matrix, models_dict, laws):
    keys = list(models_dict.keys())
    compare_results = []
    for i in range(recall_matrix.shape[0]):
        am = numpy.argmax(recall_matrix[i])
        if (am.dtype == numpy.int64):
            reg_mod = models_dict[keys[am]].localRegressionModel
            rule_coef_vec = numpy.array([reg_mod.intercept_] + list(reg_mod.coef_))
            true_coef_vec = numpy.array([laws[i]['intercept']] + laws[i]['coefs_vec'])
            angle = vector_angle(true_coef_vec, rule_coef_vec)
            compare_results.append((am, recall_matrix[i][am], angle))
        else:
            print() #several rules cover the larger part of the original law
            # this should never happen
    return sorted(compare_results)