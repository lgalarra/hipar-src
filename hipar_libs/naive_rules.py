import pandas
import numpy

from matplotlib import pyplot as plt

import sparse_linear



'''
Return the subset of the data matching the given pattern
'''
def match_pattern_df(df, pattern):
    tmpdf = pandas.DataFrame(df)
    for k in pattern:
        tmpdf = tmpdf.loc[tmpdf[k] == pattern[k]]
    
    #tmpdf = tmpdf.dropna(axis=0)
    return tmpdf 



'''
Return a rule (pattern, model, error), meant to be used with the apply(axis=1) function of a DataFrame
'''
def find_rule(s, data, target, num_attr=None):
    
    if num_attr is not None:
        tna = num_attr.copy()
        tna.remove(target)
    
    val = s.to_dict()
    
    support = val.pop('support', None)
    
    pattern = {}
    for k in val.keys():
        if val[k] is not None:
            pattern[k] = val[k]

    tmpdf = match_pattern_df(data, pattern)
    
    if tmpdf.shape[0] == 0:
        return None
    
    #model, error = sparse_linear.auto_omp_dicho(tmpdf, target, max_error_aug=2.0)
    model,error,yval,ypred = sparse_linear.find_linear_regression(tmpdf, target, tna, polynomial=False, method='ompcv')
    
    if model is None:
        return None
    
    rule = (pattern, model, error, support)
    return rule



def add_default_model(models, data, target):
    num_attr = list(data.select_dtypes(include=['float64']))
    
    #model,error,yval,ypred = sparse_linear.find_linear_regression(data, target, num_attr, polynomial=False, method='ompcv')
    model, error = sparse_linear.auto_omp_dicho(data, target, max_error_aug=2.0)
    
    rule = ({}, model, error, data.shape[0])
    m = models.copy()
    m.append(rule)
    
    return m



#-------------------------
# Pretty print functions
#-------------------------

# TODO: Use attributes name instead for the tests
def pretty_attr(a, v):
    v = v.replace(a, '')
    if ('and' in v) and ('>=' in v) and ('<' in v):
        spl = v.split(' and ')
        vinf = round(float(spl[0][2:]),4)
        vsup = round(float(spl[1][1:]),4)
        s = '(' + str(vinf) + '<=' + a + '<' + str(vsup) + ')'
        return s
    elif ('>=' in v):
        #s = str(round(float(v[2:]),4)) + '<=' + a
        s = a + '>=' + str(round(float(v[2:]),4))
        return s
    elif ('<' in v):
        s = a + '<' + str(round(float(v[1:]),4))
        return s
    else:
        return a + ':' + v


def pretty_pattern(p: dict):
    if p == {}:
        return '{Default}'
    
    s = '{'
    
    for k in p.keys():
        #ts = str(k) + ':' + str(p[k])
        ts = pretty_attr(k, p[k])
        s += ts + ' & '
    
    s = s[:-3]
    s += '}'
    return s



def pretty_linreg(model, num_lin, num_quadra):
    #used_vars = []

    if hasattr(model, 'intercept_') :
        inter = model.intercept_
        if type(inter) is numpy.ndarray:
            inter = inter[0]
        inter = round(inter, 6)
    else :
        inter = 0

        
    s = str(inter)
    coefs = model.coef_ if hasattr(model, 'coef_') else model.feature_importances_
    if len(coefs.shape) > 1:
        coefs = coefs.ravel()
    num_attr = num_lin if len(coefs) == len(num_lin) else num_quadra
    for i in range(len(coefs)):
        if coefs[i] != 0:
            c = round(coefs[i], 6)
            var = num_attr[i]
            #used_vars.append(var)
            s += ' + ' + str(c) + '*' + str(var)
    
    return s



def pretty_rules(models, num_lin, num_quadra, target):
    s = ''
    
    for m in models:
        ps = pretty_pattern(m[0])
        ls = ''
        
        if m[1] is not None:           
            ls = pretty_linreg(m[1], num_lin, num_quadra)
        else:
            ls = 'No linear model'

        #if m[0] == {} :
        #    ts = ps + ' ===> ' + ls
        #else :
        ts = ps + ' ===> ' + ls + ' : ' + str(target) + ' = ' + str(round(m[4],4)) + '\n\t | ERROR = ' + str(round(m[5],4)) \
             + ' ---- SUPP : ' + str(round(m[3]*100, 2)) + '%' #+ ' || ERROR : ' + str(m[2]) + ' ---- SUPP : ' + str(round(m[3]*100, 2)) + '%'
        s += ts + '\n'
    
    return s

def pretty_attr_html(a, v):
    new_a = '<i>' + a + '</i>'
    v = v.replace(a, '')
    if ('and' in v) and ('>=' in v) and ('<' in v):
        spl = v.split(' and ')
        vinf = round(float(spl[0][2:]),4)
        vsup = round(float(spl[1][1:]),4)
        s = '(' + str(vinf) + '<=' + new_a + '<' + str(vsup) + ')'
        return s
    elif ('>=' in v):
        #s = str(round(float(v[2:]),4)) + '<=' + a
        s = new_a + '>=' + str(round(float(v[2:]),4))
        return s
    elif ('<' in v):
        s = new_a + '<' + str(round(float(v[1:]),4))
        return s
    else:
        return new_a + ':' + v


def pretty_pattern_html(p: dict):
    et = '<font color="red"><b> ET </b></font> '
    
    if p == {}:
        return 'Aucune condition (couvre toutes les données)\n'
    
    s = ''
    
    for k in p.keys():
        #ts = str(k) + ':' + str(p[k])
        ts = pretty_attr_html(k, p[k])
        s += ts + et
    
    s = s[:-len(et)]
    s += '\n'
    return s


def pretty_linreg_html(model, num_lin, num_quadra):
    #used_vars = []
    if hasattr(model, 'intercept_') :
        inter = model.intercept_
        if type(inter) is numpy.ndarray:
            inter = inter[0]
        inter = round(inter, 6)
    else:
        ## It is a GBR model
        inter = 0
        
    s = str(inter)
    coefs = model.coef_ if hasattr(model, 'coef_') else model.feature_importances_
    if len(coefs.shape) > 1:
        coefs = coefs.ravel()
    num_attr = num_lin if len(coefs) == len(num_lin) else num_quadra
    for i in range(len(coefs)):
        if coefs[i] != 0:
            c = round(coefs[i], 6)
            var = num_attr[i]
            #used_vars.append(var)
            s += ' + ' + str(c) + '*' + str(var)
    
    return s


# c = (dict(current_model[0][0]), reg, error, supp, mean_pred, mae, res, list(current_inds), is_quadratic)
#           0                       1    2      3       4       5    6         7                8  
def get_dates(data, model):
    tmpdf = data.iloc[model[7]]
    datemin = tmpdf['date_obs'].min()
    datemax = tmpdf['date_obs'].max()
    ax = tmpdf["date_obs"].groupby([tmpdf["date_obs"].dt.year, tmpdf["date_obs"].dt.month]).count().plot(kind="bar")
    
    s = '<b>Dates couvertes :</b> \n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;du ' + str(datemin.day) + '/' + str(datemin.month) + '/' + str(datemin.year)
    s += '\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;au ' + str(datemax.day) + '/' + str(datemax.month) + '/' + str(datemax.year) + '\n'
    
    return s, ax

def get_communes(data, model):
    tmpdf = data.iloc[model[7]]
    communes = tmpdf['commune_parcelle'].unique()
    
    s = '<b>Communes concernées : </b>'
    for c in communes:
        s += c + ', '
    s += '\n'
    return s

def pretty_rules_html(data, models, num_lin, num_quadra, target, error_metric):
    tmpdf = data.copy()
    tmpdf['date_obs'] = pandas.to_datetime(tmpdf['date_obs'])    
    
    s = '<!DOCTYPE html>\n'
    s += '<html>\n'
    s += '<head>\n'
    s += '<meta charset="utf-8"/>\n'
    s += '<title>Règles hybrides symboliques et numériques</title>\n'
    s += '</head>\n'
    
    s += '<body>\n'
    s += '<h1>Sélection de ' + str(len(models)) + ' règles pour la variable ' + target + '</h1>\n'
    
    i = 1
    for m in models:
        s += '<br><br><br><br>'
        s += '<h2> Règle no ' + str(i) + ' </h2><br>\n'
        s += '<h3> Condition : </h3>\n'
        s += pretty_pattern_html(m[0]) + '<br>'
        s += '<h3> Régression </h3>\n'        
        if m[1] is not None:           
            s += target + ' = ' + pretty_linreg_html(m[1], num_lin, num_quadra) + '<br><br>\n'
        else:
            s += 'No linear model<br><br>\n'
            
        dates, ax = get_dates(tmpdf, m)
        s += dates + '<br>'
        plt.xticks(rotation=50)
        ax.figure.savefig('./html_images/' + str(i) + '.png')
        s += '<img src="./html_images/' + str(i) + '.png" alt="Dates distributions">\n<br><br>'
        s += get_communes(tmpdf, m) + '<br>'
            
        s += '<b>Valeur moyenne prédite :</b> ' + str(round(m[4],4)) + '<br>\n'
        s += '<b>' + error_metric + '</b> : ' + str(round(m[5],4)) + '<br>\n'
        s += '<b>Support : </b>' + str(round(m[3]*100, 2)) + '% (' + str(len(m[7])) + '/' + str(data.shape[0]) + ')<br>\n'
        
        i += 1
        
    s += '</body>\n'
    s += '</html>'
    
    del tmpdf
    return s


# c = (dict(current_model[0][0]), reg, error, supp, mean_pred, mae, res, list(current_inds), is_quadratic)
#           0                       1    2      3       4       5    6         7                8  