These are the sources of the datasets.

abalone, cpu, houses, mpg2001, servo, strikes, yatch: UCI repository, https://archive.ics.uci.edu/ml/index.php
beerconsumption: https://www.kaggle.com/dongeorge/beer-consumption-sao-paulo/downloads/beer-consumption-sao-paulo.zip/2
admission: https://www.kaggle.com/mohansacharya/graduate-admissions/downloads/graduate-admissions.zip/2 (we use v1.1)
concrete: https://www.kaggle.com/maajdl/yeh-concret-data/downloads/yeh-concret-data.zip/1
winequality: https://www.kaggle.com/rajyellow46/wine-quality
fuelconsumption: https://www.kaggle.com/anderas/car-consume
swiss health insurance: https://www.kaggle.com/comparisdata/premium-prediction/home
optical interconnection: https://www.kaggle.com/inancigdem/optical-interconnection-network
carbon nanotubes: https://www.kaggle.com/inancigdem/carbon-nanotubes#carbon_nanotubes.csv
